from libraries.uArm.classUarm import rack,uArmRobot,dictLetters2Num,dictNum2Letters
from libraries.backlight.classBacklight import backlight
from libraries.camera.classCamera import webCam
import json
import pandas as pd

# --------------
# json file reading
# ---------------------
jsonConfigFile = 'config/webApp_config.json'
print("Loading webApp configuration json file: ",jsonConfigFile)
df_config = pd.read_json(jsonConfigFile)

configFileRackSamples = df_config.loc['configFileRackSamples'][0]
configFileBacklight = df_config.loc['configFileBacklight'][0]
configFileiPath_1 = df_config.loc['configFileiPath_1'][0]
dict_mappingCoordinates = df_config.loc['mapping_Opentron2uArm'][0]
backlightPort = df_config.loc['backlightPort'][0]
camPort = df_config.loc['camPort'][0]
picturesPath = df_config.loc['picturesPath'][0]

# --------------
# uArm connection
# ---------------------
uArm1 = uArmRobot(effector = 'gripper')

rack1 = rack(nCols=10,nRows=4,diameter=14,name='samples')
rack2 = rack(nCols=1,nRows=1,diameter=14,name='iPath_1')
rack3 = rack(nCols=1,nRows=1,diameter=14,name='backlight')
rack1.loadConfig(filename=configFileRackSamples)
rack2.loadConfig(filename=configFileiPath_1)
rack3.loadConfig(filename=configFileBacklight)

uArm1.connect()

# --------------
# backlight connection
# ---------------------
bl = backlight(port=backlightPort)
bl.connect()

# --------------
# cam connection
# ---------------------
cam = webCam(camId=camPort,pathPictures=picturesPath)
cam.connect()

# Main !
###
vialsLocations_list = ['A1', 'B1','C1','D1']
for vialsLocations in vialsLocations_list:

    uArm1.moveZ(zTarget=rack1.ZPos['top'])
    uArm1.pickVial(rack1,pos=vialsLocations)
    uArm1.moveXY(rack2)
    if (bl.isConnected == True):
        if (bl.lighting(1) == True):
            print("Turn On the backlight")  
        else:
            print("Failed to turn On the backlight")
    uArm1.moveXY(rack3)
    cam.configCam()
    cam.keyCapture()
    uArm1.moveXY(rack2)
    if (bl.lighting(0) == True):
        print("Turn Off the backlight") 
    else: print("Failed to turn Off the backlight")
    uArm1.dropVial(rack1,pos=vialsLocations)
    uArm1.swift.reset()
    uArm1.moveXY(rack2)