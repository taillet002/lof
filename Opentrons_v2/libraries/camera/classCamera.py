
import os
import time

import cv2
import numpy as np


class webCam():
    def __init__(self, camId=0, pathPictures="", prefixName='opentrons'):
        self.isConnected = False
        self.cam = None
        self.pathPictures = pathPictures
        self.prefixName = prefixName
        self.camId = camId

    def connect(self):
        try:
            self.cam = cv2.VideoCapture(self.camId, cv2.CAP_DSHOW)
            if self.cam.isOpened():
                self.isConnected = True
                return True
            return False
        except Exception as e:
            print("Camera - ", e)
            return False

    def capture(self, n=1, name=''):
        if self.isConnected:

            try:
                img_counter = 1
                ret, frame = self.cam.read()
                while True:

                    ret, frame = self.cam.read()
                    #cv2.imshow("test", frame)
                    if not ret:
                        return False

                    #k = cv2.waitKey(1000)

                    if img_counter > n:
                        break
                    else:
                        img_name = self.prefixName + name + \
                            '-' + str(img_counter) + '.png'
                        time.sleep(0.5)
                        print(os.path.join(self.pathPictures, img_name))
                        cv2.imwrite(os.path.join(
                            self.pathPictures, img_name), frame)
                        #print("{} written!".format(img_name))
                        img_counter += 1
                return True
            except Exception as e:
                print("Camera - ", e)
                return False
        else:
            return False

    def disconnect(self):
        try:
            self.cam.release()
            self.isConnected = False
        except Exception as e:
            print("Camera - ", e)

    def keyCapture(self):
        cv2.namedWindow("test")
        while True:
            ret, frame = self.cam.read()
            cv2.imshow("test", frame)
            if not ret:
                break
            k = cv2.waitKey(1)
            if k % 256 == 27:
                # ESC pressed
                break
        cv2.destroyAllWindows()

    def configCam(self, policy='fromUser'):
        if policy == 'fromUser':

            self.cam.set(cv2.CAP_PROP_SETTINGS, 0.0)
        else:
            self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 100000)
            self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 100000)

        # cam.set(cv2.CAP_PROP_EXPOSURE, -11)
        # cam.set(cv2.CAP_PROP_FOCUS, 25)
