
import serial


class backlight():

    def __init__(self, port):
        self.isConnected = False
        self.port = port
        self.ser = None

    def connect(self):
        # if (self.isConnected == False):
        try:
            self.ser = serial.Serial(self.port, timeout=3)
            self.isConnected = True
            return True
        except Exception as e:
            print("Backlight - ", e)
            return False
        # else: True
    # def flush(self):

    def disconnect(self):
        try:
            self.ser.close()
            self.isConnected = False
            return True
        except Exception as e:
            print("Backlight - ", e)
            return False

    def lighting(self, status=0):

        if self.isConnected:
            readVal = None
            try:
                self.ser.flush()
                if status == 1:
                    self.ser.write(b'1')
                    readVal = self.ser.read()
                else:
                    self.ser.write(b'0')
                    readVal = self.ser.read()

                if readVal[0] == 49:
                    return True

                elif readVal[0] == 48:
                    return True
                else:
                    print("ici ?", readVal, readVal[0])
                    return False
            except Exception as e:
                print("Backlight - ", e)
                return False
