
import random

import numpy as np
import pandas as pd
import requests
from opentrons import instruments, labware, robot
from opentrons.data_storage import database

metadata = {
    'protocolName': 'Custom_Labwares_Configuration',
    'author': 'Tristan Aillet <tristan.aillet@solvay.com>',
    'description': """This is protocol to configure custom labwares"""
}

# ---------------------------
# General Parameters
# --------------------------
computerip = "http://169.254.115.32"  # Ip address of the server (labtop)
outputDisplay = 'robot'

# queries to the server


def queryServer(stepName):
    """ function to send request to the server
            arg:
                stepName: string corresponding to the request command
    """

    done = False
    while not done:
        logbook('..Query to server : ' + stepName, outputDisplay)
        r = requests.post(computerip, json={'step': stepName})
        data = r.json()
        done = data['done']
        error = data['error']
    if not error:
        if stepName == 'RequestingCustomLabwaresXLS':
            logbook('...xls config file has been received', outputDisplay)
            return pd.read_json(data['listdf'])
    else:
        logbook('error with query server')
        return None


def logbook(comment='', _outputDest='robot'):
    """ function to print message with print() command in simulation mode or robot.comment() when robot is connected"""
    if _outputDest == 'robot':
        robot.comment(comment)
    else:
        print(comment)


def filteringParsXLS(dfSerie, idx, defaultVal):
    df = dfSerie.fillna(defaultVal)
    if type(df[idx]) == type('str'):
        if type(defaultVal) == type('str'):
            return df[idx]
        else:
            return defaultVal
    else:
        if type(defaultVal) == type('str'):
            return defaultVal
        else:
            return df[idx]


def configCustomLabwares(df_customLabwares):
    for idx in range(df_customLabwares['name'].size):
        try:
            name = messageAdd = df_customLabwares['name'][idx]
            if name is not None:
                grid = filteringParsXLS(
                    dfSerie=df_customLabwares['grid'], idx=idx, defaultVal='1;1').split(';')
                grid = [int(i) for i in grid]
                spacing = filteringParsXLS(
                    dfSerie=df_customLabwares['spacing'], idx=idx, defaultVal='88;127.5').split(';')
                spacing = [float(i) for i in spacing]
                depth = float(filteringParsXLS(
                    dfSerie=df_customLabwares['depth'], idx=idx, defaultVal=50))
                diameter = float(filteringParsXLS(
                    dfSerie=df_customLabwares['diameter'], idx=idx, defaultVal=10))
                volume = float(filteringParsXLS(
                    dfSerie=df_customLabwares['volume'], idx=idx, defaultVal=1))*1000
                if name in labware.list():
                    database.delete_container(name)
                labware.create(
                    name,  # name of you labware
                    grid=grid,        # number of (columns, rows)
                    # distances (mm) between each (column, row)
                    spacing=spacing,
                    diameter=diameter,         # diameter (mm) of each well
                    depth=depth,           # depth (mm) of each well
                    volume=volume)         # volume (µL) of each well

                logbook('...sucessful configuring labware ' +
                        name, outputDisplay)
        except Exception as e:
            print(e)
            logbook('error when loading')


logbook('## Loading excel file...', outputDisplay)
df_customLabwares = queryServer('RequestingCustomLabwaresXLS')
configCustomLabwares(df_customLabwares)
#pipette = instruments.P300_Single(mount='right')

# for c in robot.commands():
#   print(c)
