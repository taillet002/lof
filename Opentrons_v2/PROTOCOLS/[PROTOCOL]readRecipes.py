
import random
import socket

import numpy as np
import requests
from opentrons import instruments, labware, robot
from opentrons.data_storage import database
from opentrons.util.vector import Vector

metadata = {
    'protocolName': 'Recipes_Reader',
    'author': 'Tristan Aillet <tristan.aillet@solvay.com>',
    'description': """This is protocol to prepare samples in the Actizone project"""
}

# ---------------------------
# General Parameters
# --------------------------
outputDisplay = 'robot'  # where to display robot.comments() or print()
#hostname = socket.gethostname()
#IPAddr = socket.gethostbyname(hostname)
# print(IPAddr)
# computerip = 'http://169.254.168.188' #Ip address of the server (labtop)
#computerip = 'http://169.254.168.188'
computerip = 'http://10.139.42.15'
#computerip = 'http://'+IPAddr

# ---------------------------
# Functions
# --------------------------

# to display message (or log in txt file in the future)


def logbook(comment='', _outputDest='robot'):
    """ function to print message with print() command in simulation mode or robot.comment() when robot is connected"""
    if _outputDest == 'robot':
        robot.comment(comment)
    elif _outputDest == 'none':
        pass
    else:
        print(comment)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False

def ErrMessage(errId, messageAdd=''):
    """ function to display the error message according to the error idx and to execute block commands accordingly
        Args:
            errId: integer corresponding to the error idx
            messageAdd: string corresponding to specific message to send
    """
    if errId == 0:
        logbook('...ERROR : Server has returned a critical error : ' +
                messageAdd, outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + messageAdd)

    if errId == 1:
        msg = '...Error when loading labware ' + \
            messageAdd[0] + ' in position ' + messageAdd[1]
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)

    if errId == 2:
        msg = '...Error when loading Stock Solution ' + messageAdd
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)

    if errId == 3:
        msg = '...!!!!!! Warning: the Stock Solution ' + messageAdd + \
            ' in cmps_add_order variable is not present in the list of the Stock Solutions'
        logbook(msg, _outputDest=outputDisplay)

    if errId == 4:
        msg = '...!!!!!! Error when mounting pipettes on the ' + messageAdd
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)

    if errId == 5:
        msg = '...Error when preparing CommanList with: ' + messageAdd
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 6:
        msg = '...Error when generating commands for the robot '
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 7:
        msg = '...Error when loading the custom rack : ' + messageAdd
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 8:
        msg = '...!!!!!! Warning with the command : ' + messageAdd
        logbook(msg, _outputDest=outputDisplay)
    if errId == 9:
        msg = '...!!!!!! Failed to execute ' + messageAdd
        logbook(msg, _outputDest=outputDisplay)
    if errId == 10:
        msg = '...!!!!!! Error to generate command because stockSolution ' + \
            messageAdd + ' is no present in the stock solution definition'
        logbook(msg, _outputDest=outputDisplay)
        raise NameError('Protocol is stopped due to :    ' + msg)


# queries to the server
def queryServer(stepName, pars: dict = {}):
    """ function to send request to the server
            arg:
                stepName: string corresponding to the request command
                pars: pars a additional arguments if required
    """
    done = False
    while not done:
        logbook('...Query to server : ' + stepName, outputDisplay)
        r = requests.post(computerip, json={'step': stepName, 'pars': pars})
        data = r.json()
        done = data['done']
        criticalError = data['criticalError']

    if not criticalError:

        if stepName == 'StopUArmRobot':
            logbook('... disconnecting uArm Robot ', outputDisplay)
            return data['commandExec']

        elif stepName == 'RequestingPicture':
            logbook('... taking picture ', outputDisplay)
            return data['commandExec']

        elif stepName == 'RequestingLabwares':
            return _configBlock_labwares(data['dict_df'])

        elif stepName == 'RequestingStockSolutions':
            return _configBlock_stockSolutions(data['dict_df'])

        elif stepName == 'RequestingCommandsList':
            return data['dict_df']

        elif stepName == 'RequestingParameters':
            return data['dict_df']
        else:
            logbook('... this option is not available ', outputDisplay)
    else:
        ErrMessage(0, messageAdd=data['errDescription'])
        return None
# change flow rate ratio for dispense/transfer and all

# change tip position (not use, deprecated !)
def setTipImmersion(z, refPointLabware, refPointWell, labwareObj):
    deltaPoint = refPointLabware
    h = refPointWell[2]
    coord = list(deltaPoint.to_tuple())
    coord[2] = refPointLabware[2] + z
    deltaPoint = Vector(coord)
    labwareObj._coordinates = deltaPoint
    for w in labwareObj.wells():
        w.properties['height'] = h-z
    return labwareObj

# block of commands to be executed on the robot


def _initBlock():
    """ function to execute block commands at the robot initialization"""
    logbook('## Robot initialization...', outputDisplay)
    # robot.connect()
    logbook('.. Robot is connecting...', outputDisplay)
    # robot.clear_commands()
    robot.home()
    # if (robot.is_connected()):

    # else:
    #    robot.clear_commands()
    #    robot.home()


def _endBlock():
    """ function to execute block commands when formulations preparation are finished"""
    logbook('## End of the protocol.', outputDisplay)
    robot.home()


def take(dict_, idx0):
    return dict_[str(idx0)]


# block of commands to configure the robot
def _configBlock_labwares(dict_labwares):
    """ function to load the labwares for the opentron
        args:
            df_labwares : dataframe with the labwares definition
    """

    # def setTipImmersion(z,refPointLabware,refPointWell,labwareObj):
    logbook('## Labwares configuring...', outputDisplay)
    labwareItems = {}
    #df_labwares['tipPos_aspiring'] = df_labwares['tipPos_aspiring'].fillna(0)
    #df_labwares['tipPos_dispensing'] = df_labwares['tipPos_dispensing'].fillna(0)

    for idx in range(len(dict_labwares['rackModel'])):
        name = take(dict_labwares['rackName'], idx)
        try:
            if name != 'None':
                obj = labware.load(take(dict_labwares['rackModel'], idx), take(
                    dict_labwares['rackPos'], idx))
                tipDict = {'refCoordLabware': obj._coordinates,
                           'refCoordWell': obj.wells()[0].top()[1]}
                labwareItems[name] = LabwaresDef(name, obj, tipDict)
                logbook('...sucessful loading of labware ' + name + ' in position ' +
                        str(take(dict_labwares['rackPos'], idx)), outputDisplay)

        except Exception as e:
            robot.comment(e)
            ErrMessage(1, messageAdd=(
                name, str(take(dict_labwares['rackPos'], idx))))
    return labwareItems


def _configBlock_stockSolutions(dict_stockSolutions):
    """ function to configure the stocksolutions (vials, rackname etc.)
    args:
        df_stockSolutions : dataframe with the stock solutions definitions
    """
    logbook('## Stock Solutions configuring...', outputDisplay)

    stockSolutionsItems = {}
    for idx in range(len(dict_stockSolutions['name'])):
        try:
            name = take(dict_stockSolutions['name'], idx)
            if name != 'None':
                stockSolutionsItems[name] = StockSolutions(name, take(dict_stockSolutions['rackName'], idx), take(
                    dict_stockSolutions['vialsPosition'], idx), take(dict_stockSolutions['mixBeforeAdding'], idx), take(dict_stockSolutions['%A'], idx))
                logbook('...sucessful loading of Stock Solution ' + stockSolutionsItems[name].name + ' in rack ' +
                        stockSolutionsItems[name].rackName + ' in position ' + stockSolutionsItems[name].vialsPosition, outputDisplay)
        except Exception as e:
            robot.comment(e)
            ErrMessage(2, messageAdd=(name))
    return stockSolutionsItems


def _configBlock_pipettes(df_parameters):
    """ function to configure the pipettes
    args:
        df_parameters : dataframe with the general parameters to configure the robots
        Need LabwareItems to run !
    """
    logbook('## Pipettes configuring...', outputDisplay)
    pipetteItems = Pipettes()
    #

    pipettesRightModel = df_parameters['parsValue']['pipette_right_model']
    pipettesLeftModel = df_parameters['parsValue']['pipette_left_model']

    pipettesLeftQ = df_parameters['parsValue']['pipette_left_flowrates']
    pipettesRightQ = df_parameters['parsValue']['pipette_right_flowrates']
    idx = 0
    if pipettesRightModel != 'None':
        rackPipettesList = df_parameters['parsValue']['pipette_right_rack']
        try:
            if rackPipettesList == 'None':
                rackObjList = []
            else:
                rackObjList = [
                    labwareItems[i].obj for i in rackPipettesList.split(",")]
            pipetteItems.setObj(idx, pipettesRightModel,
                                'right', rackObjList, pipettesRightQ)
            logbook('...sucessful pipettes mounting on the right position: model ' + pipettesRightModel +
                    ' and attached to the following tip racks: ' + df_parameters['parsValue']['pipette_right_rack'], outputDisplay)
            idx = 1
        except Exception as e:
            robot.comment(e)
            ErrMessage(4, messageAdd='right')

    if pipettesLeftModel != 'None':
        rackPipettesList = df_parameters['parsValue']['pipette_left_rack']
        try:
            if rackPipettesList == 'None':
                rackObjList = []
            else:
                rackObjList = [
                    labwareItems[i].obj for i in rackPipettesList.split(",")]
            pipetteItems.setObj(idx, pipettesLeftModel,
                                'left', rackObjList, pipettesLeftQ)
            logbook('...sucessful pipettes mounting on the left position: model ' + pipettesLeftModel +
                    ' and attached to the following tip racks: ' + df_parameters['parsValue']['pipette_left_rack'], outputDisplay)
        except Exception as e:
            robot.comment(e)
            ErrMessage(4, messageAdd='left')
    return pipetteItems


def _displayCommandsList(commandsList):
    for idx in range(len(commandsList['rackSource'])):
        try:
            if commandsList['commandName'][idx] == 'Adding':
                logbook('C' + str(idx) + ': ' + commandsList['commandName'][idx] + ' - ' + commandsList['stockSolutionName'][idx] + ' from (rack: ' + commandsList['rackSource'][idx] + ' / wells: ' + str(commandsList['wellsSource'][idx]) + ') to (rack: ' + commandsList['rackDest'][idx] + ' / wells: ' + str(commandsList['wellsDest'][idx]) + ') | pipette: ' + str(
                    commandsList['pipette'][idx]) + ' (vol. (ul): ' + str(commandsList['volDest'][idx]) + ' | tip location: (src: ' + str(commandsList['rackSrcPosTip'][idx]) + ' dest: ' + str(commandsList['rackDestPosTip'][idx]) + ') FlowRates ratio: ' + str(commandsList['ratioFlowrates'][idx]), outputDisplay)
            elif commandsList['commandName'][idx] == 'Mixing':
                logbook('C' + str(idx) + ': ' + commandsList['commandName'][idx] + ' - ' + ' on vials (rack: ' + commandsList['rackSource'][idx] + ' / wells: ' + str(commandsList['wellsSource'][idx]) + ') | pipette: ' + str(
                    commandsList['pipette'][idx]) + ' (vol. (ul): ' + str(commandsList['volDest'][idx]) + ' / repeat: ' + str(commandsList['rep'][idx]) + ' / changeTip: ' + str(commandsList['changeTip'][idx]) + ')', outputDisplay)

            elif commandsList['commandName'][idx] == 'Waiting':
                logbook('C' + str(idx) + ': ' + commandsList['commandName'][idx] + ' - ' + str(
                    commandsList['stockSolutionName'][idx]) + ' seconds', outputDisplay)

            elif commandsList['commandName'][idx] == 'TakingPictures':
                logbook('C' + str(idx) + ': ' + commandsList['commandName'][idx] + ' -  take pictures of vial (rack: ' + commandsList['rackDest'][idx] + ' / well: ' + str(
                    commandsList['wellsDest'][idx]) + ' / Mixing: ' + commandsList['custom1'][idx] + ')', outputDisplay)
            elif commandsList['commandName'][idx] == 'DropletsDeposing':
                logbook('C' + str(idx) + ': ' + commandsList['commandName'][idx] + ' -  deposing droplets from (rack: ' + commandsList['rackSource'][idx] + ' / well: ' + str(
                    commandsList['wellsSource'][idx]) + ') to (rack: ' + commandsList['rackDest'][idx] + ' / wells: ' + str(commandsList['wellsDest'][idx]) + ') | pipette: ' + str(
                    commandsList['pipette'][idx]) + ' (vol. (ul): ' + str(commandsList['volDest'][idx]) + ') | FlowRates ratio: ' + str(commandsList['ratioFlowrates'][idx]) + ' | Mixing: ' + commandsList['custom1'][idx], outputDisplay)
        
        except Exception as e:
            robot.comment(e)
            ErrMessage(8, messageAdd=str(idx))


def _executeCommandsList(df_parameters, pipetteItems, labwareItems, stockSolutionsItems, commandsList):
    def pipetteTipUpdate(newNameStockSolution, pipetteId, changePipette=0):
        # check if the current stockSol is the same or not as the one used  in iter-1
        if not newNameStockSolution == lastNameStockSolution[pipetteId]:
            changePipette = 1  # set 1 the tip needs to be changed

        if changePipette:
            # check if this is the first time the pipette is used. In this case, no need to drop the tip
            if not lastNameStockSolution[pipetteId] == 'reset':
                #last_pipetteId = commandsListDistribute['pipette'][idx0-1]
                if trashPolicy == 'return':
                    pipetteItems.obj[pipetteId].return_tip()
                else:
                    pipetteItems.obj[pipetteId].drop_tip()
                pipetteItems.obj[pipetteId].pick_up_tip()
            else:
                pipetteItems.obj[pipetteId].pick_up_tip()

            lastNameStockSolution[pipetteId] = newNameStockSolution

    def waitingCommandsExecution(command, stepIter):
        time2wait = command['stockSolutionName']
        pipetteItems.obj[0].delay(seconds=time2wait)

    def wellsObj(listRack, rackObj, height=0, locTip='bottom'):
        _wellsObj = []
        for iWells in listRack:
            if (is_number(iWells)):
                iWells = int(iWells)

            if locTip == 'bottom':
                _wellsObj.append(rackObj.wells(iWells).bottom(float(height)))
            else:
                _wellsObj.append(rackObj.wells(iWells).top(float(height)))
        return _wellsObj

    def addingCommandsExecution(command, stepIter):

        # Read the command items from the list
        pipetteId = command['pipette']
        newNameStockSolution = command['stockSolutionName']
        vol = command['volDest']
        rackSourceObj = labwareItems[command['rackSource']].obj
        rackDestObj = labwareItems[command['rackDest']].obj
        # Change tip position in the vial
        
        #listSrcWells = command['wellsSource'].split(",")
        listSrcWells = str(command['wellsSource']).split(",")

        #robot.comment(listSrcWells)
        listDestWells = command['wellsDest']
        heightSrc = command['rackSrcPosTip']
        heightDest = command['rackDestPosTip']
        mixing_rate = command['ratioFlowrates']
        mix_before = None
        if command['rep'] is not None:
            rep = command['rep']
            volMix = pipetteItems.obj[pipetteId].max_volume
            # command['changeTip']
            mix_before = (rep, volMix)
        # Pipettes handle
        # Initialization of the function to use

        logbook('## Sequence of distributing of the stock solution: ' +
                newNameStockSolution, outputDisplay)
        pipetteTipUpdate(newNameStockSolution, pipetteId)

        if dispense_method == 'distribute':
            logbook('...sequence will use the distribute method ', outputDisplay)
            dispenseLiq = pipetteItems.obj[pipetteId].distribute
        else:
            logbook('...sequence will use the transfer method ', outputDisplay)
            dispenseLiq = pipetteItems.obj[pipetteId].transfer

        # change pipette ratio
        aspFlowrate_Ref = pipetteItems.aspFlowrates[pipetteId]
        dispFlowrate_Ref = pipetteItems.dispFlowrates[pipetteId]

        aspFlowrate_New = int(aspFlowrate_Ref * mixing_rate)
        dispFlowrate_New = int(dispFlowrate_Ref * mixing_rate)

        # logbook(str(type(aspFlowrate_New)),outputDisplay)
        pipetteItems.obj[pipetteId].set_flow_rate(
            aspirate=aspFlowrate_New, dispense=dispFlowrate_New)
        # Define policy : try to sample in all the sources vials equally
        # Here, we split the list of rackDest because opentrons crash if rackSrc cannot divide rackDest...        # Apply general policy if many wells in the rackSource

        if len(listSrcWells) > 1:
            # Random to not start always with the same vial
            random.shuffle(listSrcWells)
            # If the destWells list is higher than the source, we need to split to be divided
            if len(listDestWells) >= len(listSrcWells):
                # find the integer that allows division to split accordingly
                r = int(len(listDestWells)/len(listSrcWells))
                # find the number of wells to consider in the destRack
                nWellsDest = int(r*len(listSrcWells))
                # Split 1 : first split will have a number of dest Wells that can be divided by the number of wells in source
                listDestWells_split1 = listDestWells[0:nWellsDest]
                # Split 2 : Second split will have the remaining number of dest wells (less or equal than the source wells number)
                listDestWells_split2 = listDestWells[nWellsDest:]
                vol_split1 = vol[0:nWellsDest]  # idems split for volumes
                vol_split2 = vol[nWellsDest:]

                # dispenseLiq(vol_split1, rackSourceObj.wells(listSrcWells), rackDestObj.wells(listDestWells_split1),trash=False,new_tip=tipChangePolicy,mix_before=mix_before) #do the distribution
                dispenseLiq(vol_split1, wellsObj(listSrcWells, rackSourceObj, heightSrc, 'bottom'), wellsObj(
                    listDestWells_split1, rackDestObj, heightDest, 'top'), trash=False, new_tip=tipChangePolicy, mix_before=mix_before)

                if listDestWells_split2:  # if this list is not empty; do the distribution
                    listSrcWells_split2 = listSrcWells[0:len(
                        listDestWells_split2)]
                    dispenseLiq(vol_split2, wellsObj(listSrcWells_split2, rackSourceObj, heightSrc, 'bottom'), wellsObj(
                        listDestWells_split2, rackDestObj, heightDest, 'top'), trash=False, new_tip=tipChangePolicy, mix_before=mix_before)
            # if hte destWells list is smaller than the source, we only take the exact number of wells in the src list (randomly selected)
            else:
                listSrcWells_split2 = listSrcWells[0:len(listDestWells)]
                dispenseLiq(vol, wellsObj(listSrcWells_split2, rackSourceObj, heightSrc, 'bottom'), wellsObj(
                    listDestWells, rackDestObj, heightDest, 'top'), trash=False, new_tip=tipChangePolicy, mix_before=mix_before)

        # If only one wells in the source, we add it in the dest well
        else:
            dispenseLiq(vol, wellsObj(listSrcWells, rackSourceObj, heightSrc, 'bottom'), wellsObj(
                listDestWells, rackDestObj, heightDest, 'top'), trash=False, new_tip=tipChangePolicy, mix_before=mix_before)
        # We set original flowartes
        pipetteItems.obj[pipetteId].set_flow_rate(
            aspirate=aspFlowrate_Ref, dispense=dispFlowrate_Ref)

    def mixingCommandsExecution(command, stepIter):
        pipetteId = command['pipette']
        newNameStockSolution = command['stockSolutionName']
        mixing_rate = command['ratioFlowrates']
        vol = command['volDest']
        rep = command['rep']
        changeTip = command['changeTip']
        rackDestObj = labwareItems[command['rackDest']].obj
        listDestWells = command['wellsDest']
        heightDest = command['rackDestPosTip']
        logbook('## Sequence of mixing steps: ' +
                newNameStockSolution, outputDisplay)
        # pipetteTipUpdate(newNameStockSolution,pipetteId,changePipette=0)
        for well in listDestWells:
            #if (c == 0): pipetteTipUpdate(newNameStockSolution,pipetteId,changePipette=0)
            # else:
            if changeTip == 'yes':
                pipetteTipUpdate(newNameStockSolution,
                                 pipetteId, changePipette=1)
            else:
                pipetteTipUpdate(newNameStockSolution, pipetteId)
            for h in heightDest:
                pipetteItems.obj[pipetteId].mix(rep, vol, rackDestObj.wells(
                    well).bottom(float(h)), rate=mixing_rate)

    def takingPicturesCommandsExecution(command, stepIter):
        # Check robot connection()

        vialsLocations = command['wellsDest']
        rackName = command['rackDest']
        pipetteItems.obj[0].move_to(robot.fixed_trash.well(0).top())
        # if (queryServer('StartUArmRobot') == True):
        return queryServer('RequestingPicture', pars={'rackName': rackName, 'vialsLocations': vialsLocations})

    # read parameters
    dispense_method = df_parameters['parsValue']['dispense_method']

    # df_parameters['parsValue']['mixing_rate']
    trashPolicy = df_parameters['parsValue']['trash_policy']
    tipChangePolicy = 'never'
    # global variables initialization
    # need to change pipettes or not ?
    lastNameStockSolution = ['reset', 'reset']

    # Dispense commandsListDistribute
    for idx0, commandName in enumerate(commandsList['commandName']):

        listKeys = list(commandsList.keys())
        command = {listKeys[idx1]: commandsList[listKeys[idx1]][idx0]
                   for idx1 in range(len(listKeys))}
        try:
            if commandName == 'Adding':
                #pass
                #trashPolicy = command['custom1']
                addingCommandsExecution(command, stepIter=idx0)
            elif commandName == 'Mixing':
                mixingCommandsExecution(command, stepIter=idx0)
            elif commandName == 'Waiting':
                waitingCommandsExecution(command, stepIter=idx0)
            #elif commandName == 'DropletsDeposing':
                
            elif commandName == 'TakingPictures':
                if robot.is_simulating():
                    logbook('Taking picture on vials ' +
                            str(command['wellsDest']), outputDisplay)
                else:
                    if takingPicturesCommandsExecution(command, stepIter=idx0):
                        logbook('Taking picture on vials ' +
                                str(command['wellsDest']), outputDisplay)
                    else:
                        logbook('Failed to take picture of vials ' +
                                str(command['wellsDest']), outputDisplay)
        except Exception as e:
            robot.comment(e)
            ErrMessage(9, messageAdd='command ' +
                       str(idx0) + ' (' + commandName + ')')
            return False

    # Returning pipette

    for i in range(2):
        if pipetteItems.obj[i] is not None:
            if trashPolicy == 'return':
                pipetteItems.obj[i].return_tip()
            else:
                pipetteItems.obj[i].drop_tip()
    return True


class StockSolutions():
    def __init__(self, name, rackName, vialsPosition, mixBeforeAdding, concentration):
        self.name = name
        self.rackName = rackName
        self.vialsPosition = vialsPosition
        self.mixBeforeAdding = mixBeforeAdding
        self.concentration = concentration

    def asdict(self):
        return {'name': self.name, 'rackName': self.rackName, 'vialsPosition': self.vialsPosition, 'mixBeforeAdding': self.mixBeforeAdding, 'concentration': self.concentration}


class LabwaresDef():
    def __init__(self, name, labwareObject, tipDict):
        self.name = name
        self.obj = labwareObject
        self.tipPos = tipDict
        #{depthRef:None, tipPosDispensing:None, tipPosAspiring:None}


class Pipettes():
    def __init__(self):
        self.obj = {0: None, 1: None}
        self.aspFlowrates = {0: None, 1: None}
        self.dispFlowrates = {0: None, 1: None}

    def setObj(self, idx, modelName, mountLoc, rackObjList, flowrates):
        flowrates = flowrates.split(',')
        defPipettesList_method = {'P50_Single': instruments.P50_Single, 'P10_Single': instruments.P10_Single,
                                  'P300_Single': instruments.P300_Single, 'P1000_Single': instruments.P1000_Single}
        defPipettesList_flowrates = {'P50_Single': (25, 50), 'P10_Single': (
            5, 10), 'P300_Single': (150, 300), 'P1000_Single': (500, 1000)}
        if flowrates[0] == 'None':
            listFlowrates = defPipettesList_flowrates[modelName]
        else:
            listFlowrates = [float(flow) for flow in flowrates]
        self.obj[idx] = defPipettesList_method[modelName](
            mount=mountLoc, tip_racks=rackObjList, aspirate_flow_rate=listFlowrates[0], dispense_flow_rate=listFlowrates[1])
        self.aspFlowrates[idx] = listFlowrates[0]
        self.dispFlowrates[idx] = listFlowrates[1]

    def selectPipette(self):
        if self.obj[1] is None:
            valsplit = 0
            valsplitIdxMax = 0
        else:
            valList = [self.obj[0].max_volume, self.obj[1].max_volume]
            valsplit = min(valList)
            valsplitIdxMin = valList.index(min(valList))
            valsplitIdxMax = valList.index(max(valList))
        return valsplit, valsplitIdxMin, valsplitIdxMax

    def asdict(self):
        valsplit, valsplitIdxMin, valsplitIdxMax = self.selectPipette()
        return {'valsplit': valsplit, 'valsplitIdxMin': valsplitIdxMin, 'valsplitIdxMax': valsplitIdxMax, 'volumesMax': (self.obj[valsplitIdxMin].max_volume, self.obj[valsplitIdxMax].max_volume)}


def _connectRobot():
    logbook('## Connecting uArm Robot...', outputDisplay)
    if queryServer('StartUArmRobot'):
        logbook('... the uArm robot is connected', outputDisplay)
        return True
    else:
        logbook('... the uArm robot failed to connect', outputDisplay)
        return False


# ---------------------------------------
# Sequence of commands() for formulation preparations
# --------------------------------------
_initBlock()  # initialization of the robot
labwareItems = queryServer('RequestingLabwares')
stockSolutionsItems = queryServer('RequestingStockSolutions')
parameters = queryServer('RequestingParameters')

pipetteItems = _configBlock_pipettes(parameters)
_stockSolutionsItems = {}
for k in stockSolutionsItems.keys():
    _stockSolutionsItems[k] = stockSolutionsItems[k].asdict()

commandsList = queryServer('RequestingCommandsList', pars={
                           'stockSolutionsItems': _stockSolutionsItems, 'pipettesDict': pipetteItems.asdict()})
_displayCommandsList(commandsList)
executeCommandsList = _executeCommandsList(
    parameters, pipetteItems, labwareItems, stockSolutionsItems, commandsList)

logbook('## Disonnecting uArm Robot...', outputDisplay)
if not robot.is_simulating():
    queryServer('StopUArmRobot')
