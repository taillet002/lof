%This function reads raw signal from Avantes spectrometer and returns
%absorbance.
function val=function_ReadAvantes(spectro)
val = nan;
if ~isempty(spectro.dark) && ~isempty(spectro.ref)
    spectro.acquire();
    temp = spectro.get_data();
    %Reading only wavelengths of interest (611-613 nm for blue dye, i.e. cells
    %713:717; 351-354 nm for I3- ions, i.e. cells 273:278
    acq = temp.data(1,713:717)' - spectro.dark';
    val = mean(real(-log10(acq./spectro.ref')));
end
end