% ------- Class acquisition ---------
% 
%    Tristan AILLET, le 05 Janv. 2018
% -----------------------------------
%
% class_acquisition(F,frequence,[options]);
% F : function handle, par ex. F = @() function_Test(2) 
% frequence: nombre
% option: - xlabel 'str' - ylabel 'str'- legend {'str1' 'str2'}
%
% ~~~~~~~~~~~ Exemple ~~~~~~~~~~
% 
% acq1 = class_acquisition(@() f_Test(2,3),1,['xlabel','x','ylabel','y','legend',{'plot1' 'plot2'}]);
%
% acq1.start(); # lance le Timer
% acq1.open_figure(); # Ouvre le plot 
% acq1.open_figure(ax); #dessine dans l'objet ax donn� en arg.
% acq1.X # matrice des valeurs acquises (ligne 1 => temps, ligne >1 => valeurs)
% acq1.stop(); # stop le timer
% acq1.export(path); # export la matrice acq1.X au format excel
%
%
classdef class_acquisition < handle
    properties
       
        t; %objet timer
        f; %objet figure
        ax; %objet axe
        fhandle; %nom de la fonction a utilisee
        
        firstTime;
        graphProperties = {'' '' '' {''}};
        list_sym = {'bx' 'go' 'm*' 'b+' 'gs' 'cd'}
        X = []; %matrice du temps et des valeurs lues
    end
    methods
        function obj=class_acquisition(fhandle,freq,varargin)
            
            obj.fhandle = fhandle;
            
            obj.t = timer;
            obj.firstTime = 0;
 
            obj.t.TimerFcn = @(x,y) obj.fcallback();
            obj.t.Period = 1/freq;
            obj.t.ExecutionMode = 'fixedRate';
            obj.t.TasksToExecute = inf;
            obj.t.BusyMode = 'drop';
            
            if (nargin > 2)
                
                list_pars = [3:2:nargin];
                for i1=1:length(list_pars)
                    if (list_pars(i1) < nargin)
                        s = varargin{list_pars(i1)-2};
                        switch s
                            case 'xlabel'
                                obj.graphProperties(1) = varargin(list_pars(i1)+1-2);
                            case 'ylabel'
                                obj.graphProperties(2) = varargin(list_pars(i1)+1-2);
                            case 'title'
                                obj.graphProperties(3) = varargin(list_pars(i1)+1-2);
                            case 'legend'
                                obj.graphProperties(4) = varargin(list_pars(i1)+1-2);    
                            otherwise
                                disp('erreur, propri�te non reconnue');
                        end
                    else
                        disp('erreur ans les arguments de la fonction');
                    end
                end
            end
        end
        function obj=fcallback(obj)
            val = feval(obj.fhandle);
            obj.accumulate(val);
            obj.display_plot();
        end
        function r=getf(obj)
           r=feval(obj.fhandle);
        end
        function display_plot(obj)
            if (ishandle(obj.ax))
               % set(0,'CurrentFigure',obj.f);
                %cla
                
                if ~isempty(obj.X)
                    temp = ceil(length(obj.X(:,1))/length(obj.list_sym(1,:)));
                    list_symb = repmat(obj.list_sym,1,temp);
                    temp = obj.X(1,:)./60;
                    cla(obj.ax)
                    for i1=2:length(obj.X(:,1))
                         plot(obj.ax,temp,obj.X(i1,:),cell2mat(list_symb(i1-1)));
                         hold(obj.ax,'on')
                    end
                    try
                        delta_value = (max(max(obj.X(2:end,:))) - min(min(obj.X(2:end,:))));
                        if ~(delta_value == 0)
                            ylim(obj.ax,[min(min(obj.X(2:end,:)))-delta_value*0.1 max(max(obj.X(2:end,:)))+delta_value*0.1]);
                        end 
                    end
                    ylabel(obj.ax,obj.graphProperties{2});
                    xlabel(obj.ax,obj.graphProperties{1});
                    %legend(obj.ax,obj.graphProperties{4});
                    
                end
            end
        end
		function obj=export(obj,path)
			if ~isempty(obj.X)
                temp0(1) = {'temps (s)'};
				for i1=2:length(obj.X(:,1))
					temp0(i1) = {strcat('col ',num2str(i1))};
				end
				temp1 = mat2cell(obj.X',ones(1,size(obj.X',1)),ones(1,size(obj.X',2)));
			end
			temp2 = [temp0;temp1];
			xlswrite(path,temp2);
		end
        function obj=reset(obj)
                obj.X = [];
        end
        function obj=open_figure(obj,varargin)
                if (nargin > 1)
                    obj.ax = varargin{1};
                else
                    if (isempty(ishandle(obj.ax)) || ~ishandle(obj.ax))
                        figure;
                        obj.ax = axes();
                    end
                    
                end 

            hold on
        end
        function delete(obj)
            delete(obj.t);
        end
        function obj=delete_timer(obj)
            delete(obj.t);
        end
        function start(obj)
            if ~(obj.firstTime)
               tic;
               obj.firstTime = 1;
            end
            try
                start(obj.t);
            catch
                disp('Erreur lors du d�mmarage du timer (raison => d�j� en route, ou autre)');
            end
        end
        function stop(obj)
            stop(obj.t);
        end
        function obj=accumulate(obj,val)
            temp = toc;
            obj.X(1:length(val)+1,end+1) = [temp;val];
        end
        
        
       end
end