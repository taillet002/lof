function val=function_ReadDigPressureHoneywell(arduino,addr,rangeP,type)
    val = arduino.I2C_requestFrom(addr,2,1);
    if (strcmp(type,'A'))                                                                                                
        OutputMax = 14745;                                                                             
        OutputMin = 1638;                                                                              
        Output = bitand(val(1),63);                                                                     

        Output = bitshift(Output,8) + val(2);                                                           
        Pressure = (((Output -OutputMin)*(rangeP(2)-rangeP(1)))/(OutputMax-OutputMin))+rangeP(1);       
    end   
    val = Pressure+1;
end