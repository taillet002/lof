% coeffs= [coeff1_capteur1 coeff1_capteur2 ...; coeff_2_capteur1 ....]
% coeff_2*x+coeff_1
function val=function_ReadAnalogPressureHoneywell(arduino,pins,coeffs)    
    for i1=1:length(pins)     
       b = coeffs(1,i1);
       a = coeffs(2,i1);
       val(i1,1) = a*arduino.analogRead(pins(i1))+b; 
    end
end