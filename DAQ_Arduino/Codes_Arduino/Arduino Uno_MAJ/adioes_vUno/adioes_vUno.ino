
/* Analog and Digital Input and Output Server for MATLAB     */
/* Giampiero Campa, Copyright 2012 The MathWorks, Inc        */

/* This file is meant to be used with the MATLAB arduino IO 
   package, however, it can be used from the IDE environment
   (or any other serial terminal) by typing commands like:
   
   0e0 : assigns digital pin #4 (e) as input
   0f1 : assigns digital pin #5 (f) as output
   0n1 : assigns digital pin #13 (n) as output   
   
   1c  : reads digital pin #2 (c) 
   1e  : reads digital pin #4 (e) 
   2n0 : sets digital pin #13 (n) low
   2n1 : sets digital pin #13 (n) high
   2f1 : sets digital pin #5 (f) high
   2f0 : sets digital pin #5 (f) low
   4j2 : sets digital pin #9 (j) to  50=ascii(2) over 255
   4jz : sets digital pin #9 (j) to 122=ascii(z) over 255
   3a  : reads analog pin #0 (a) 
   3f  : reads analog pin #5 (f) 

   5j    : reads status (attached/detached) of servo on pin #9
   5k    : reads status (attached/detached) of servo on pin #10
   6j1   : attaches servo on pin #9
   8jz   : moves servo on pin #9 of 122 degrees (122=ascii(z))
   7j    : reads angle of servo on pin #9
   6j0   : detaches servo on pin #9
   
   E0cd  : attaches encoder #0 (0) on pins 2 (c) and 3 (d)
   E1st  : attaches encoder #1 on pins 18 (s) and 19 (t)
   E2vu  : attaches encoder #2 on pins 21 (v) and 20 (u)
   G0    : gets 0 position of encoder #0
   I0u   : sets debounce delay to 20 (2ms) for encoder #0
   H1    : resets position of encoder #1
   F2    : detaches encoder #2
   
   R0    : sets analog reference to DEFAULT
   R1    : sets analog reference to INTERNAL
   R2    : sets analog reference to EXTERNAL
  
   X3    : roundtrip example case returning the input (ascii(3)) 
   99    : returns script type (0 adio.pde ... 3 motor.pde ) */
#include <SPI.h>
#include <SPI_Matlab.h>
#include <Wire.h>

//Change card frequency for PWM
//DuePWM pwm;
int PWM_freq1;
int PWM_freq2;
int PWM_pinSelect;


const unsigned long SPI_speedMaximum = 14000000;
uint8_t SPI_dataOrder = 0;
uint8_t SPI_dataMode = 0;
uint8_t SPI_data[5] = {0,0,0,0,0}; 

uint8_t I2C_byte = 0;
uint8_t I2C_addr = 0; 
uint8_t I2C_number = 0;

unsigned int val3 = 0;

/* Gestion du SPI */
int SPI_number = 0;
bool SPI_sens = true; //true => MSB first
SPI_Matlab SPI_[10];

/* define internal for the MEGA as 1.1V (as as for the 328)  */
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define INTERNAL INTERNAL1V1
#endif

void setup() {
  //analogWriteResolution(12); 
  //analogReadResolution(12);
  Wire.begin();
  Serial.begin(115200);
}

void loop() {  
  /* variables declaration and initialization                */
  
  static int  s   = -1;    /* state                          */
  static int  pin = 13;    /* generic pin number             */
  static int  enc = 0;     /* generic encoder number         */
  
  int nSPI = 0;
  int  val =  0;           /* generic value read from serial */
  double val2 = 0;
  
  int  temp_val = 0;
  int  agv =  0;           /* generic analog value           */
  int  dgv =  0;           /* generic digital value          */

  byte data = 0; 
  uint8_t dataBuffer;

  if (Serial.available() >0) {

    val = (int) Serial.read();

    switch (s) {  
      case -1:      
        /* calculate next state                                */
        if (val>47 && val<90) {
          s=10*(val-48);
        }
      
        if ((s>200 && s<210) || (s>250 && s!=340 && s!=400)) {
          s=-1;
        }
          break; 

        /* s=0 or 1 means CHANGE PIN MODE                      */
        case 0:
        /* the second received value indicates the pin 
           from abs('c')=99, pin 2, to abs('¦')=166, pin 69    */
        if (val>98 && val<167) {
          pin=val-97;                /* calculate pin          */
          s=1; /* next we will need to get 0 or 1 from serial  */
        } 
        else {
          s=-1; /* if value is not a pin then return to -1     */
        }
        break; /* s=0 taken care of                            */


        case 1:
        /* the third received value indicates the value 0 or 1 */
        if (val>47 && val<50) {
          /* set pin mode                                      */
          if (val==48) {
            pinMode(pin,INPUT);
          }
          else {
            pinMode(pin,OUTPUT);
          }
        }
        s=-1;  
        break; 
     /* Configuration des ports I2C */
        case 100: // I2C
          if (val>97 && val<103) {
            if (val == 98) { //beginTransmission
                s=110;
              } 
              else if (val == 99) { //READ
                s = 115;
              }
              else if (val == 100) { //endTransmission
                s = 120;
              }
              else if (val == 101) { //write
                s = 125;
              }
              else if (val == 102) { //write
                s = 126;
              }
              else { s = -1; }
            }
          break;      

        case 110: //Wire.beginTransmission(address)
         if (val>97 && val<110) {
            I2C_number = val-98; 
            s = 1101;
          }
          else { s= -1;}
          break;
          
        case 1101: // Start 2 (bit2read)
         if (val>97 && val<100) { 
            I2C_byte = val-98;
            s = 1102;
          }
          else { s= -1;}
          break;
          
        case 1102: 
           I2C_addr = val;
           
           Wire.beginTransmission(I2C_addr);
           Serial.println(I2C_addr,DEC);
           s = -1;
          break;
          
        case 115: // Wire.read() 
         if (val>97 && val<110) {
            I2C_number = val-98; 
            s = 1151;
          }
          else { s= -1;}
          break;
          
        case 1151: 
         if (val>97 && val<100) { 
            I2C_byte = val-98;
            if (Wire.available()) {
              Serial.println(Wire.read(),DEC);
            }
            else {
              Serial.println(0,DEC);
            }
            s = -1;
          }
          break;

        case 120: //Wire.endTransmission(stop)
         if (val>97 && val<110) {
            I2C_number = val-98; 
            Serial.println(Wire.endTransmission(I2C_number),DEC);
            s = -1;
          }
          break;
        case 125: //write 1 (ID)
         if (val>97 && val<110) {
            I2C_number = val-98; 
            s = 1251;
          }
          else { s = -1; }
          break;
          
        case 1251: // Write 2 (bit2read)
            I2C_byte = val;
            Wire.write(I2C_byte); 
            Serial.println(val,DEC);
            s = -1;
          break;
          
        case 126: //Wire.requestFrom(address, quantity, stop)
            I2C_addr = val; 
            s = 1261;
          
          break;          
        case 1261: 
         if (val>97 && val<110) {
            I2C_number= val-98; 
            s = 1262;
          }
          else { s= -1;}
          break;  
        case 1262: //Wire.requestFrom(address, quantity, stop)
         if (val>97 && val<110) {
            I2C_byte= val-98; 
            Serial.println(Wire.requestFrom(I2C_addr, I2C_number, I2C_byte),DEC);
            s = -1;
          }
          else { s= -1;}
          break;  
// GESTION PWM
        case 130: //Gestion d'un PWM
            if (val>97 && val<113) { 
              if (val == 98) { // Configure Frequency
                s=1301;
              }
              else if (val == 99) { // Set Pin number to frequency 1 or 2
                s = 1302;
              }
              else if (val == 100) { // Digital write on pin number
                s = 1303;
              }
              else if (val == 101) { // pin STOP
                s = 1304;
              }
              else { s = -1; }
            } 
            break;
        case 1301: //configure Frequency
              PWM_freq1 = val;
              s=13011;
            break;
        case 13011: //configure Frequency
              PWM_freq2 = val;
              //pwm.setFreq1(PWM_freq1);
              //pwm.setFreq2(PWM_freq2);
              Serial.println(1,DEC);
              s=-1;           
            break;
        case 1302: //Set pin number to freq1 or 2
             if (val - 98)
             {
                s=13021; 
               }
             else
             {
                s=13022;
               } 
            break;
        case 13021: //Set pin number to freq1
            if (val>97 && val<161) { // 
                //pwm.pinFreq1(val-98);
            } 
            Serial.println(val-98,DEC);
            s = -1;
            break;
        case 13022: //Set pin number to freq2 
            if (val>97 && val<161) { // 
                //pwm.pinFreq2(val-98);
            } 
            Serial.println(1,DEC);
            s = -1;
            break;
        case 1303: //Digital write on pin number
            if (val>97 && val<161) { // 
              PWM_pinSelect = val-98;
              s=13031;
            } 
            break;
        case 13031: //Digital write on pin number
            //pwm.pinDuty(PWM_pinSelect, val);
            Serial.println(PWM_pinSelect,DEC);
            s=-1;
            break;
        case 1304: //STOP pin
            if (val>97 && val<161) { // 
              PWM_pinSelect = val-98;
              s=13041;
            } 
            break;
        case 13041: //STOP PIN
            //pwm.stop(PWM_pinSelect);
            Serial.println(1,DEC);
            s=-1;
            break;

 /* Utilisation de la librairie SPI */
        case 180: // SPI Librairie
             if (val>97 && val<113) { 
              if (val == 98) { // Begin Transaction
               
                s=1801;
              }
              else if (val == 99) { // End transaction 
                s = 1802;
              }
              else if (val == 100) { // ShiftIn
                s = 1803;
              }
              else if (val == 101) { // ShiftOut
                s = 1804;
              }
              else if (val == 102) { // Begin & Stop
                s = 1805;
              }
              else { s = -1; }
            }      
            break;
        case 1801:
          if (val>97 && val<110) {
             SPI_dataOrder = (val-98);
             s = 18011;
             }
          break;
        case 18011:
          if (val>97 && val<110) {
             SPI_dataMode = (val-98);
             switch (SPI_dataMode) {
                case 0:
               //   SPI.beginTransaction(SPISettings(SPI_speedMaximum, SPI_dataOrder, SPI_MODE0));
                break;
                case 1:
               //   SPI.beginTransaction(SPISettings(SPI_speedMaximum, SPI_dataOrder, SPI_MODE1));
                break;
                case 2:
                 // SPI.beginTransaction(SPISettings(SPI_speedMaximum, SPI_dataOrder, SPI_MODE2));
                break;
                case 3:
                 // SPI.beginTransaction(SPISettings(SPI_speedMaximum, SPI_dataOrder, SPI_MODE3));
                break;
              }
             Serial.println(1,DEC); 
             s = -1;
             }             
          else { s = -1; }
          break; 
          
          case 1802: // End transaction
            s = -1;
            SPI.endTransaction(); 
           break;
           
          case 1803: // ShiftIn DataPin
             SPI_data[0] = val;
             s = 18031;
             break;
          case 18031: // ShiftIn DataclockPin
            SPI_data[1] = val;
            s = 18032;
            break;
          case 18032: // ShiftIn bitOrder
            SPI_data[2] = val;
            s = -1;
           
            SPI_data[3] = shiftIn(SPI_data[0],SPI_data[1],SPI_data[2]);
            Serial.println(SPI_data[3],DEC);
           // Serial.println(,DEC);
            break;

          case 1804: // ShiftOut DataPin
             SPI_data[0] = val;
             s = 18041;
             break;
          case 18041: // ShiftOut DataclockPin
            SPI_data[1] = val;
            s = 18042;
            break;
          case 18042: // ShiftOut bitOrder
            SPI_data[2] = val;
            s = 18043;
            break;
          case 18043: // ShitOut Bit2Send
            SPI_data[3] = val;
            shiftOut(SPI_data[0],SPI_data[1],SPI_data[2],SPI_data[3]);
            Serial.println(1,DEC);
            s = -1;
            break;
            
          case 1805: // Start & Begin
            if (val == 98) {
               SPI.begin();          
               }
            else { 
              SPI.end();
            }
            s = -1; 
            break;
            
        
 /* Configuration des ports SPI */
        case 160: // SPI
          if (val>97 && val<110) {
            if (val == 98) { 
                s=1601;
              }
              else if (val == 99) {
                s = 1602;
              }
              else if (val == 100) {
                s = 1603;
              }
              else if (val == 101) {
                s = 1604;
              }
              else if (val == 102) {
                s = 1605;
              }
              else { s = -1; }
            }
          break;      
        case 1601: // Menu 1 pour paramétrer le SPI
         if (val>97 && val<120) {
            SPI_number = val-98; 
            s = 161;
          }
          else { s= -1;}
          break;           
        case 1602: // Menu 2 pour mettre le SPI en marche
            SPI_number = val-98; 
            SPI_[SPI_number].OnOff = true; 
            s = -1;
            break;  
        case 1603: // Menu 3 pour mettre leSPI en off
            SPI_number = val-98; 
            SPI_[SPI_number].OnOff = false; 
            s = -1;
            break;          
         case 1604: // Menu 4 pour lire le SPI
            SPI_number = val-98; 
            s = 162;
            break;             
         case 1605: // Menu 5 renvoie le nombre de SPI connectés et ensuite continu dans menu pour renvoyer valeurs
           nSPI = 0;
           for (int e1=0;e1<10;e1++)
            {
                if (SPI_[e1].OnOff)
                {
                  nSPI = nSPI+1;
                }
                
            }
            Serial.println(nSPI,DEC);  
            //if (nSPI == 0) { s = -1; }
             s = 163; 
            
            break;        
            
         case 161: // SPI => on parametre CLK
            SPI_[SPI_number].pin_CLK = val;
            s = 1610;
            break;
         case 1610: // SPI => on parametre CS
            SPI_[SPI_number].pin_CS = val;
            s = 1611;
            break; 
         case 1611: // SPI => on parametre MISO
            SPI_[SPI_number].pin_MISO = val;
            s = 1612;
            break; 
         case 1612: // SPI => on parametre setBitFirst
            SPI_[SPI_number].setBitOrder = val;
            s = 1613;
            break; 
           case 1613: // SPI => on parametre setDataMode
            SPI_[SPI_number].setDataMode = val;
            s = 1614;
            break;   
           case 1614: // SPI => on parametre nb_bytes2read (ne sert que si readout_type = 0)
            SPI_[SPI_number].nb_bytes2read = val;
            s = 1615;
            break;  
           case 1615: // SPI => on parametre readseect
            SPI_[SPI_number].readSelect = val;
            SPI_[SPI_number].set();
            Serial.println(1,DEC);
            s = -1;
            break;  
            
          case 162: // SPI => on lit la valeur
            val2 = 0;
            if (SPI_[SPI_number].OnOff)
            {
              val2 = SPI_[SPI_number].read(SPI_[SPI_number].readSelect);
              Serial.println(val2);
            }
            else {
              Serial.println(val2);
            }
            s = -1;
            break; 
 
         case 163: // SPI => on lit toutes les valeur 
            for (int e1=0;e1<10;e1++)
            {
                if (SPI_[e1].OnOff)
                {
                  val2 = SPI_[e1].read(SPI_[e1].readSelect);
                  //Serial.println(3);
                  Serial.println(val2);
                }
            }
            s = -1;
            break;

      /* s=10 means DIGITAL INPUT ************************** */
      
      case 10:
      /* the second received value indicates the pin 
         from abs('c')=99, pin 2, to abs('¦')=166, pin 69    */
      if (val>98 && val<167) {
        pin=val-97;                /* calculate pin          */
        dgv=digitalRead(pin);      /* perform Digital Input  */
        Serial.println(dgv);       /* send value via serial  */
      }
      s=-1;  /* we are done with DI so next state is -1      */
      break; /* s=10 taken care of                           */

      

      /* s=20 or 21 means DIGITAL OUTPUT ******************* */
      
      case 20:
      /* the second received value indicates the pin 
         from abs('c')=99, pin 2, to abs('¦')=166, pin 69    */
      if (val>98 && val<167) {
        pin=val-97;                /* calculate pin          */
        s=21; /* next we will need to get 0 or 1 from serial */
      } 
      else {
        s=-1; /* if value is not a pin then return to -1     */
      }
      break; /* s=20 taken care of                           */

      case 21:
      /* the third received value indicates the value 0 or 1 */
      if (val>47 && val<50) {
        dgv=val-48;                /* calculate value        */
	      digitalWrite(pin,dgv);     /* perform Digital Output */
      }
      s=-1;  /* we are done with DO so next state is -1      */
      break; /* s=21 taken care of                           */

	
      /* s=30 means ANALOG INPUT *************************** */
      
      case 30:
      /* the second received value indicates the pin 
         from abs('a')=97, pin 0, to abs('p')=112, pin 15    */
      if (val>96 && val<113) {
        pin=val-97;                /* calculate pin          */
        agv=analogRead(pin);       /* perform Analog Input   */
	Serial.println(agv);       /* send value via serial  */
      }
      s=-1;  /* we are done with AI so next state is -1      */
      break; /* s=30 taken care of                           */



      /* s=40 or 41 means ANALOG OUTPUT ******************** */
      
      case 40:
      /* the second received value indicates the pin 
         from abs('c')=99, pin 2, to abs('¦')=166, pin 69    */
      if (val>98 && val<167) {
        pin=val-97;                /* calculate pin          */
        s=41; /* next we will need to get value from serial  */
      }
      else {
        s=-1; /* if value is not a pin then return to -1     */
      }
      break; /* s=40 taken care of                           */


      case 41:
      /* the third received value indicates the analog value */
        val3 = (val)<<8;
        s=42; 
      break;
    
      case 42:
        val3 = val3 | val;
        analogWrite(pin,val3);        /* perform Analog Output  */      
       // Serial.println((int) val3,DEC);
        s=-1;  /* we are done with AO so next state is -1      */
      break; /* s=41 taken care of                           */
      
      
      
      
      /* s=90 means Query Script Type: 
         (0 adio, 1 adioenc, 2 adiosrv, 3 motor)             */
      
      case 90:
      if (val==57) { 
        /* if string sent is 99  send script type via serial */
        Serial.println(0);
      }
      s=-1;  /* we are done with this so next state is -1    */
      break; /* s=90 taken care of                           */

      


      /* s=340 or 341 means ANALOG REFERENCE *************** */
      
      case 340:
      /* the second received value indicates the reference,
         which is encoded as is 0,1,2 for DEFAULT, INTERNAL  
         and EXTERNAL, respectively. Note that this function 
         is ignored for boards not featuring AVR or PIC32    */
         
#if defined(__AVR__) || defined(__PIC32MX__)

      switch (val) {
        
        case 48:
        analogReference(DEFAULT);
        break;        
        
        case 49:
        analogReference(INTERNAL);
        break;        
                
        case 50:
        analogReference(EXTERNAL);
        break;        
        
        default:                 /* unrecognized, no action  */
        break;
      } 

#endif

      s=-1;  /* we are done with this so next state is -1    */
      break; /* s=341 taken care of                          */



      /* s=400 roundtrip example function (returns the input)*/
      
      case 400:

      Serial.println(val);

      s=-1;  /* we are done with the aux function so -1      */
      break; /* s=400 taken care of                          */



      /* ******* UNRECOGNIZED STATE, go back to s=-1 ******* */
      
      default:
      
      s=-1;  /* go back to the initial state, break unneeded */

    } /* end switch on state s                               */

  } /* end if serial available                               */
  
} /* end loop statement                                      */


