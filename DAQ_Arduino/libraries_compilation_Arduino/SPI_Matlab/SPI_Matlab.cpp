#include "Arduino.h"
#include "SPI_Matlab.h"

void SPI_Matlab::set() {

	OnOff = true;
	pinMode(pin_CLK,OUTPUT);
	pinMode(pin_CS,OUTPUT);
	pinMode(pin_MISO,INPUT);

	}
double SPI_Matlab::read(int S) {
    int i, e0;
	long int d = 0;
	switch(S) {
        case 0:
			if (setDataMode == 0) {
			  digitalWrite(pin_CLK, LOW);
			}
			else { digitalWrite(pin_CLK, HIGH);}
		  
			delay(1);
			digitalWrite(pin_CS, LOW);
			delay(1);
			for (e0=0;e0<nb_bytes2read;e0++)
			{  
			  if (setBitOrder) {
				d = d<<8;
				d |= shiftIn(pin_MISO, pin_CLK, MSBFIRST);
			  }
			  else {
				 d |= shiftIn(pin_MISO, pin_CLK,LSBFIRST)<<(e0*8);
			  }
			}
			digitalWrite(pin_CS, HIGH);
			break;
		case 1:
			digitalWrite(pin_CLK, LOW);
			delay(1);
			digitalWrite(pin_CS, LOW);
			delay(1);

			for (i=31; i>=0; i--)
			{
				digitalWrite(pin_CLK, LOW);
				delay(1);
				d <<= 1;
				if (digitalRead(pin_MISO)) {
				  d |= 1;
				}

				digitalWrite(pin_CLK, HIGH);
				delay(1);
			}
			digitalWrite(pin_CS, HIGH);
			  //Serial.println(d, HEX);	  
			if (d & 0x7) {
					// uh oh, a serious problem!
				return NAN; 
			}
			if (d & 0x80000000) {
				// Negative value, drop the lower 18 bits and explicitly extend sign bits.
				d = 0xFFFFC000 | ((d >> 18) & 0x00003FFFF);
				//d *= 0.25;
			}
			else {
				// Positive value, just drop the lower 18 bits.
				d >>= 18;
			}
			  double centigrade = d;
	
			  // LSB = 0.25 degrees C
			  centigrade *= 0.25;
			  return centigrade;
			  //centigrade;
			
			break;
	}
	return d;
}
