## DAQ Arduino 
by Tristan AILLET (LOF)
Version: v1.7a


## Codes Arduino:
Fichier *.ino permettant de piloter la carte arduino via communication RS232:
- entrées/sorties analogiques et digitales
- gestion des protocoles I2C et SPI

** les librairies nécessaires à la compilation sous arduino sont disponibles dans le dossier "libraries_compilation_Arduino". 

## Codes Matlab:
- Class arduino_('COMx'): communication en RS232 pour piloter la carte arduino
- class class_acquisition(): acquisition et stockage de données 
- class class_PID(): rétroaction régulée via PID

## Codes Python:
A développer

L'aide à l'utilisation des fonctions est disponible dans chaque fichier.

# Update sur 1.7a, Janvier 2018 (Tristan AILLET)

- Suppression du contrôle PID sur Arduino (calculs réalisés sur interface matlab (class_PID) ou autre)
- Fiabilisation du code avec gestion des exceptions (lecture/ecriture SPI et I2C)
- Cartographie des fonctions codées sous ino (dans dossier arduino)
- Epuration du code (supression moteurs et autres, => objectifs juste d'acquisition digit ou analog
- Correction bug dans acquisition + amélioration du code (fiabilisation etc.)
- Réalisation d'une documentation simplifiée sous matlab pour class_acquisition et class_PID (via "help class_PID" par ex)