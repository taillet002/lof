import os
import sys
import time
import pandas as pd
import keyboard
from uarm.wrapper import SwiftAPI
import numpy as np
import pickle

dictLetters2Num = {'A':0,'B':1,'C':2,'D':3,'E':4,'F':5,'G':6,'H':7,'I':8,'J':9,'K':10,'L':11,'M':12,'N':13,'O':14,'P':15,'Q':16,'R':17,'S':18,'T':19,'U':20,'V':21,'W':22,'X':23,'Y':24,'Z':25,
    'a':0,'b':1,'c':2,'d':3,'e':4,'f':5,'g':6,'h':7,'i':8,'j':9,'k':10,'l':11,'m':12,'n':13,'o':14,'p':15,'q':16,'r':17,'s':18,'t':19,'u':20,'v':21,'w':22,'x':23,'y':24,'z':25}

dictNum2Letters = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H', 8:'I', 9:'J', 10:'K', 11:'L', 12:'M', 13:'N', 14:'O',15:'P',16:'Q',17:'R',18:'S',19:'T',20:'U',21:'V',22:'W',23:'X',24:'Y',25:'Z'}
path = ''
filename = 'samplesRack.rack'
class rack():
    
    def __init__(self,nCols,nRows,diameter,name='defaultName'):
        self.nCols = nCols
        self.nRows = nRows
        self.name = name
        
        self.zRackGround = 5
        self.rackHeight = 30
        self.vialHeight = 60
        
        self.xs  = 0
        self.ys = 0
        #spaceBound, 0: above bottom, 1: above Top, 2: below top to grip
        self.offSets = {'bottom':0, 'top':0, 'gripIn':0, 'gripOut':0, 'transfer':0}
        self.diameter = diameter
        #p1 -- p2
        #|     |
        #p4 -- p3
        self.refPoints = pd.DataFrame(index=['p1','p2','p3','p4'], columns=['x', 'y', 'z'])
        self.defineRefPos()
        
    def defineRefPos(self):
        self.ZPos = {'top': self.vialHeight + self.offSets['top'] + self.zRackGround,
        'bottom' : self.offSets['bottom'] + self.zRackGround,
        'gripIn' : self.vialHeight + self.zRackGround + self.offSets['gripIn'],
        'gripOut' : self.vialHeight + self.zRackGround + self.offSets['gripOut'] + self.rackHeight,
        'transfer' : 2*self.vialHeight + self.zRackGround + self.offSets['transfer'] }

    def storedConfig(self,path,filename):
        filename = os.path.join(path,filename)
        savedDict = {'xs':self.xs, 'ys': self.ys, 'name':self.name,'nCols':self.nCols, 'nRows':self.nRows,'zRackGround':self.zRackGround,'rackHeight':self.rackHeight, 'vialHeight':self.vialHeight, 'offSets':self.offSets, 'diameter':self.diameter, 'refPoints':self.refPoints}
        f = open(filename,"wb")
        pickle.dump(savedDict,f)
        f.close()

    def loadConfig(self,path,filename):
        filename = os.path.join(path,filename)
        saveDict = pickle.load(open(filename, "rb" ))
        self.nCols = saveDict['nCols']
        self.nRows = saveDict['nRows']
        self.name = saveDict['name']
        self.zRackGround = saveDict['zRackGround']
        self.rackHeigh = saveDict['rackHeight']
        self.vialHeight = saveDict['vialHeight']
        self.xs  = saveDict['xs']
        self.ys = saveDict['ys']
        self.offSets = saveDict['offSets']
        self.diameter = saveDict['diameter']
        self.refPoints = saveDict['refPoints']
        self.defineRefPos()

    def setCalibrationSimulation(self):
        self.refPoints.iloc[0] = [0,10,0]
        self.refPoints.iloc[1] = [10,10,0]
        self.refPoints.iloc[2] = [10,0,0]
        self.refPoints.iloc[3] = [0,0,0]
        
    def convertCoordNum2Letters(self,rcCoord):
        return dictNum2Letters[rcCoord[0]]+str(rcCoord[1]+1)
        
    def setPosition(self,uArm,grid):
        pauseTime = 0.5
        nRows = np.linspace(0,self.nRows-1,grid[0],dtype=np.int16)
        nCols = np.linspace(0,self.nCols-1,grid[1],dtype=np.int16)
        cols,rows = np.meshgrid(nCols,nRows)
        idx = 0
        coords = [[[0,0,0] for i in range(len(nCols))] for i in range(len(nRows))]
        coords = np.array(coords)
        for rIdx,r in enumerate(rows[:,0]):
            for cIdx,c in enumerate(cols[0,:]):
                time.sleep(pauseTime)
                print('set Point P' + str(idx) + ' (coord:  ' + self.convertCoordNum2Letters((r,c)) +')')                
                uArm.moveManually()
                posi = uArm.swift.get_position()
                coords[rIdx,cIdx] = posi  # ajout colonne au dataframe
                print('coord :',posi)        
                idx += 1        
        self.refPoints = coords
        time.sleep(pauseTime)
        print('set Ground reference for Z axis')
        uArm.moveManually()
        posi = uArm.swift.get_position()
        self.zRackGround = posi[2]
        time.sleep(pauseTime)
        
        print('set Top vial reference for Z axis')
        uArm.moveManually()
        posi = uArm.swift.get_position()
        self.vialHeight = posi[2]-self.zRackGround
        time.sleep(pauseTime)


        print('set Top rack reference for Z axis')
        uArm.moveManually()
        posi = uArm.swift.get_position()
        self.rackHeigh = posi[2]-self.zRackGround
        time.sleep(pauseTime)
        
        # Creation of the mapping to function to get vials coordinates
        self.setMappingPoints(cols,rows,coords)
        
        # Rest home position of the robot
        uArm.swift.reset(speed=uArm.speed, wait=True)
        
    def setMappingPoints(self,mapCols,mapRows,mapCoords):
        # positions rack definition
        xs,  ys = np.meshgrid(np.linspace(0, 1, self.nCols), np.linspace(0, 1, self.nRows))        
        
        # On complete les lignes connues
        for rIdx,r in enumerate(mapRows[:,0]):
            for cIdx,c in enumerate(mapCols[0,:]):
                
                if (cIdx < len(mapCols[0,:])-1):
                    nSteps = mapCols[0,cIdx+1] - c + 1
                    # ,mapCoords[rIdx,mapCols[0,cIdx+1],0]
                    print(mapCoords[rIdx,cIdx,0],mapCoords[rIdx,cIdx+1,0],nSteps)
                    xs[r,c:mapCols[0,cIdx+1]+1] = np.linspace(mapCoords[rIdx,cIdx,0],mapCoords[rIdx,cIdx+1,0],nSteps)
                    ys[r,c:mapCols[0,cIdx+1]+1] = np.linspace(mapCoords[rIdx,cIdx,1],mapCoords[rIdx,cIdx+1,1],nSteps)
        
        for cIdx in range(self.nCols):
            for rIdx,r in enumerate(mapRows[:,0]):
                if (rIdx < len(mapRows[:,0])-1):
                    print(rIdx,cIdx)
                    nSteps = mapRows[rIdx+1,0] - r + 1
                    xs[r:mapRows[rIdx+1,0]+1,cIdx] = np.linspace(xs[r,cIdx],xs[mapRows[rIdx+1,0],cIdx],nSteps)
                    ys[r:mapRows[rIdx+1,0]+1,cIdx] = np.linspace(ys[r,cIdx],ys[mapRows[rIdx+1,0],cIdx],nSteps)

        self.xs = xs
        self.ys = ys
        self.defineRefPos()
        
    def well(self,w):
        col = int(w[1:])-1
        row = dictLetters2Num[w[0]]
        return [self.xs[row,col],self.ys[row,col]]
            
class uArmRobot():
            
    def __init__(self,effector):
        self.swift = None
        self.device_info = None
        self.speed = 0
        self.device_info = None
        self.effector = effector
        
    def connect(self):
        self.swift = SwiftAPI(filters={'hwid': 'USB VID:PID=2341:0042'})#port="COM17"
        self.swift.waiting_ready(timeout=3) #waiting time to connect before timeout
        self.device_info = self.swift.get_device_info()
        print(self.device_info)
        firmware_version = self.device_info['firmware_version']
        if firmware_version.startswith(('0.', '1.', '2.', '3.')):
            self.speed = 50000
        else:
            self.speed = 10
        self.swift.set_mode(0) #mode normal (par défaut = 0, 1 = lser mode, 2 = 3D printing mode etc.
        self.swift.reset(speed=self.speed, wait=True)
        time.sleep(2)
        # set offset because not straight
        self.swift.set_wrist(angle=90, wait=True) #replie le bras à 90° ?

    def disconnect(self):
        
        self.swift.reset(wait=True)
        time.sleep(1)
        # clear all the commands before disconnecting
        self.swift.flush_cmd(wait_stop=True)
        time.sleep(2)
        self.swift.disconnect()
        
    def getInfo(self):
        
        if (self.swift.connected()):
            print(self.swift.get_device_info())
            print(self.swift.get_power_status())
            print(self.swift.get_limit_switch())
            print(self.swift.get_gripper_catch())
            print(self.swift.get_pump_status())
            print(self.swift.get_mode())
            print(self.swift.get_servo_attach(servo_id=0))
            print(self.swift.get_servo_attach(servo_id=1))
            print(self.swift.get_servo_attach(servo_id=2))
            print(self.swift.get_servo_attach(servo_id=3))
            print(self.swift.get_servo_angle())
            print(self.swift.get_polar())
            print(self.swift.get_position())
            print(self.swift.get_analog(0))
            print(self.swift.get_digital(0))
            #effector = input("Enter end effector type:-> gripper OR pump OR holder: ")
            
    def toggleGripper(self):
        state = self.swift.get_gripper_catch()
        #return self.swift.get_gripper_catch()
        if state == 0:
            self.swift.set_gripper(catch=True, check=True, wait=True)
            time.sleep(2)
        else:
            self.swift.set_gripper(catch=False, check=True, wait=True)
            time.sleep(2)
    
    def motorsUnlock(self):
        self.swift.set_servo_detach(0, wait=True)
        self.swift.set_servo_detach(1, wait=True)
        self.swift.set_servo_detach(2, wait=True)
    
    def motorsLock(self):
        self.swift.set_servo_attach(0, wait=True)
        self.swift.set_servo_attach(1, wait=True)
        self.swift.set_servo_attach(2, wait=True)

    def moveTo(self,rack,pos='A1',wellZPos='top'):
        #get pos
        originPos = self.swift.get_position()
        destPos = rack.well(pos)
        destPos.append(originPos[2])
        destPos[2] = rack.ZPos[wellZPos]
        print('... uArm is moving to pos ',pos,' in rack ',rack.name,' coordinates :',destPos)
        self.swift.set_position(x=round(destPos[0],4), y=round(destPos[1],4),z=round(destPos[2],4), speed=self.speed,wait=True)
        
    def moveXY(self,rack,pos='A1'):
        #get pos
        originPos = self.swift.get_position()
        destPos = rack.well(pos)
        destPos.append(originPos[2])
        print('... uArm is moving to pos ',pos,' in rack ',rack.name,' coordinates :',destPos)
        self.swift.set_position(x=round(destPos[0],4), y=round(destPos[1],4),z=round(destPos[2],4), speed=self.speed,wait=True)

    def moveZ(self,rack,zTarget,accurate=False,stepsN=3): #Change Z Position
        originPos = self.swift.get_position()
        if (accurate):
            seqSteps = np.linspace(originPos[2],zTarget,stepsN)
        else: 
            seqSteps = [zTarget]
        for zCoord in seqSteps:
            originPos[2] = zCoord
            print('... uArm is moving to ccoordinates :',originPos)
            self.swift.set_position(x=round(originPos[0],4), y=round(originPos[1],4),z=round(originPos[2],4), speed=self.speed,wait=True)        

    def pickVial(self,rack,pos='A1'):
        #check if grip is open or not
        if (self.swift.get_gripper_catch()==0):
            
            # get current position and check heigh versus top() 
            coord = self.swift.get_position()
            zSrc = coord[2]
            zDest = rack.ZPos['transfer']
            
            # We move to the transfer Z pos if necessary
            self.moveZ(rack,zTarget=max([zSrc,zDest]))
            
            # We move to the rack x,y coordinates
            self.moveXY(rack,pos=pos)
            # We move down to the
            #self.moveZ(rack,zTarget=rack.ZPos['top'])
            self.moveZ(rack,zTarget=rack.ZPos['gripOut'],accurate=True,stepsN=5)
            self.moveZ(rack,zTarget=rack.ZPos['gripIn'],accurate=True,stepsN=5)
            # We grip the vial
            self.toggleGripper()
            
            # We get out the vial
            self.moveZ(rack,zTarget=rack.ZPos['gripOut'])
            self.moveZ(rack,zTarget=rack.ZPos['transfer'])
            return True
        else:
            print('Gripper is already in use')
            return False
    def dropVial(self,rack,pos='A1'):
        if (self.swift.get_gripper_catch()!=0):
            # get current position and check heigh versus top() 
            coord = self.swift.get_position()
            zSrc = coord[2]
            zDest = rack.ZPos['transfer']

            # We move to the transfer Z pos if necessary
            self.moveZ(rack,zTarget=max([zSrc,zDest]))
           
            # We move to the rack x,y coordinates
            self.moveXY(rack,pos=pos)

            # We move to the gripOut position
            self.moveZ(rack,zTarget=rack.ZPos['gripOut'])

            # We then move to gripIn position to release gripper
            self.moveZ(rack,zTarget=rack.ZPos['gripIn'],accurate=True,stepsN=5)
            self.toggleGripper()

            #We get out the arm
            self.moveTo(rack,pos=pos,wellZPos='transfer')
            return True
        else:
            print('Nothing to drop as Gripper is opened')
            return False        
    def transferVials(self,rack,posSrc,posDest):
        pick = self.pickVial(rack,pos=posSrc)
        if (pick):
            drop = self.dropVial(rack,pos=posDest)
        else: return False
        return True

    def moveManually(self):
        while True:
            # move forward
            if keyboard.is_pressed('w'):
                self.swift.set_position(x=1, y=0, z=0, speed=self.speed,
                                wait=True, relative=True)

            # move backward
            elif keyboard.is_pressed('s'):
                self.swift.set_position(x=-1, y=0, z=0, speed=self.speed,
                                wait=True, relative=True)
            
            # move right
            elif keyboard.is_pressed('a'):
                self.swift.set_position(x=0, y=1, z=0, speed=self.speed,
                                wait=True, relative=True)
            
            # move left
            elif keyboard.is_pressed('d'):
                self.swift.set_position(x=0, y=-1, z=0, speed=self.speed,
                                wait=True, relative=True)
                
            # move up
            elif keyboard.is_pressed('up'):
                self.swift.set_position(x=0, y=0, z=1, speed=self.speed,
                                wait=True, relative=True)

            # move down
            elif keyboard.is_pressed('down'):
                self.swift.set_position(x=0, y=0, z=-1, speed=self.speed,
                                wait=True, relative=True)

            # activate gripper/pump
            elif keyboard.is_pressed('space'):
                if self.effector == "gripper":  # if gripper is attached
                    self.toggleGripper()
                elif self.effector == "pump":  # if pump is attached
                    pass
                    #self.togglePump()
                elif self.effector == "holder":
                    print("No action performed")
            # unlock all motors to manually position the arm
            elif keyboard.is_pressed('u'):
                self.motorsUnlock()
            # lock all motors to set its position
            elif keyboard.is_pressed('l'):
                self.motorsLock()
            # set the points and then break out of the while loop
            elif keyboard.is_pressed('return'):
                # set the end points of the pallet
                # return swift.get_position()
                break

if __name__ == "__main__":
    uArm1 = uArmRobot(effector = 'gripper')
    rack1 = rack(nCols=10,nRows=4,diameter=14,name='rack1')
    uArm1.connect()
    input("Press keyboard to continue")

else :
    uArm1 = uArmRobot(effector = 'gripper')
    rack1 = rack(nCols=10,nRows=4,diameter=14,name='rack1')
    uArm1.connect()