'''
Tri continent Nikon Photo with Excel workflow with uarm
'''
# import math
import os  # operating system (cmd)
# import re
import subprocess
import sys
import time
from datetime import datetime
from tkinter import Tk, filedialog  # interface fichiers / folders

import numpy as np
import pandas as pd
from openpyxl import load_workbook
from openpyxl.styles import Font, colors
from uarm.wrapper import SwiftAPI

# internal Uarm (GitHub) library
sys.path.append(os.path.join(os.path.dirname(__file__), '../../..'))

# init arm TO LAUNCH WITH F9 !!!
swift = SwiftAPI(filters={'hwid': 'USB VID:PID=2341:0042'})
swift.waiting_ready(timeout=5)
swift.reset(wait=True, speed=20)

# choose the Excel File
root = Tk()
root.withdraw()
file_name = os.path.abspath(filedialog.askopenfilename(title='Select Excel workflow',
                                                       filetypes=(("Excel file", "*.xls"),
                                                                  ("Excel file",
                                                                   "*.xlsx"),
                                                                  ("Excel file", "*.csv"))))
# root.deiconify()
# root.destroy()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                   functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


def rackpos(nRow, nCol, lim):
    # positions rack definition
    xs, ys = np.meshgrid(np.linspace(0, 1, nCol), np.linspace(0, 1, nRow))

    # x corrections
    xs[0, :] = np.linspace(lim[0][0], lim[2][0], nCol)
    xs[-1, :] = np.linspace(lim[1][0], lim[3][0], nCol)
    # pour chaque colonne on reprend les écarts (sauf la 1 et 15)
    for c in range(xs.shape[1]-1):
        xs[:, c] = np.linspace(xs[0, c], xs[-1, c], nRow,dtype=np.float16)

    # y corrections
    ys[0, :] = np.linspace(lim[0][1], lim[2][1], nCol)
    ys[-1, :] = np.linspace(lim[1][1], lim[3][1], nCol)
    for c in range(ys.shape[1]-1):
        ys[:, c] = np.linspace(ys[0, c], ys[-1, c], nRow,dtype=np.float16)

    return xs, ys


def move_to(pos=(1, 1, 100)):
    swift.set_position(x=xs[pos[0]-1, pos[1]-1],
                       y=ys[pos[0]-1, pos[1]-1],
                       z=pos[2],
                       speed=10, wait=True)  # rack wheaton home (1,1)


def lift_Vial(xdep, ydep, pos=(1, 1, 100)):
    swift.set_position(x=xs[pos[0]-1, pos[1]-1]-xdep,
                       y=ys[pos[0]-1, pos[1]-1]-ydep,
                       z=pos[2],
                       speed=10, wait=True)  # rack wheaton home (1,1)


def func_TakeNikonPicture(input_filename):
    command = (r'C:\Program Files (x86)\digiCamControl\CameraControlCmd.exe')
    command_args = (
        r'/filename {} /capture'.format(input_filename))

    full_command = command + ' ' + command_args
    p = subprocess.Popen(full_command, stdout=subprocess.PIPE,
                         universal_newlines=True)
    (output, err) = p.communicate()

    # This makes the wait possible
    p_status = p.wait(1)
    # print(p.stdout.readline())

    # This will give you the output of the command being executed
    # print('Command output: ' + str(output))
    print('Command err: ' + str(err))
    print('done')


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

# read excel workflow
df = pd.read_excel(file_name, 'workflow')
# where to save photos
path = os.path.join(os.getcwd(), "Photo")


# variables globales
z = 120    # heigh before to catch the vial

if df['Vial Size'].iloc[0] == 8:
    # variables globales
    '''
    zc = 60
    nRow = 7        # number of line of the rack
    nCol = 10       # number of col of the rack
    # x and y of corners (upper-left, bottom-left, upper-right, bottom-right)
    lim = [[145.7767, -124.5824],
           [307.7931, -126.9393],
           [135.9128, 120.9245],
           [299.8532, 127.4491]]  # rack wheaton on metalic board
    limdep = [[1,1,1],
              [2,2,2],
              [3,3,3],
              [4,4,4],
              [5,5,5],
              [6,6,6],
              [7,7,7],
              [8,8,8],
              [9,9,9],
              [10,10,10]] 
    # x and y and z movement for each coloumn (because of the angle the arm do not need to move in the same direction to lift the Vial)   
    '''
elif df['Vial Size'].iloc[0] == 20:
    # variables globales
    nRow = 5       # number of line of the rack
    nCol = 7       # number of col of the rack
    # x and y of corners (upper-left, bottom-left, upper-right, bottom-right)
    lim = [[159.1977, -112.1197],
           [308.0518, -116.6624],
           [155.6386, 118.7948],
           [306.5238, 126.9663]]  # rack wheaton on metalic board
		   
    # x, y and z movement for each coloumn (because of the angle the arm do not need to move in the same direction to lift the Vial)
    limdep = [[16, 7, 50.5],
              [16.9, 6.9, 52],
              [13.8, 10.4, 51.5],
              [12, 15, 51.5],
              [8.2, 20, 52],
              [4.7, 17, 52],
              [2.3, 19.7, 50]]
else:
    print('Vial size unknown')

xs, ys = rackpos(nRow, nCol, lim)

excel = load_workbook(file_name)
# new_excel = copy(excel) # a writable copy (I can't read values out of this, only write to it)
# the sheet to write to within the writable copy
sheet = excel.get_sheet_by_name('workflow')


# Sort data with the Global order
for dfi in df["Global Order"].unique():
    dfgo = df[df["Global Order"] == dfi]

    number_vial = 0
    # Calcul of the number of vial to know the time of the photos captures
    for k in dfgo["Vial"].unique():
        dfvial = dfgo[dfgo.Vial == k]
        number_vial = number_vial+1

    # Sort data with the Vial order
    for i in dfgo["Vial"].unique():
        dfvial = dfgo[dfgo.Vial == i]
        print('\nVial n°: ', i)

        for index, row in dfvial.iterrows():   # Photo targets on this Vial

            # Move to the Vial position
            #row_pos = int(np.mod(i-1,nRow)+1)
            #col_pos = int(np.floor((i-1)/nRow)+1)

            # new calcul : same ref as the dispensing arm
            row_pos = int(np.mod(nRow-i, nRow)+1)
            col_pos = int(np.floor((i-1)/nRow)+1)
            xdep = limdep[col_pos-1][0]
            ydep = limdep[col_pos-1][1]
            zc = limdep[col_pos-1][2]

            move_to((row_pos, col_pos, z))  # bottom of the vial
            time.sleep(1)
            # step before to catch the vial : circular mouvement of the arm
            move_to((row_pos, col_pos, zc+20))
            time.sleep(1)
            move_to((row_pos, col_pos, zc))  # catch the vial
            time.sleep(1)
            move_to((row_pos, col_pos, z))  # bottom of the vial
            time.sleep(1)

            swift.set_position(x=41.9246,
                               y=288.7331,
                               z=z,
                               speed=20, wait=True)
                               
            swift.set_position(x=41.9246,
                               y=288.7331,
                               z=79.2796,
                               speed=20, wait=True)

            # Take the picture
            rawimagename = df['Name'].iloc[int(index)]
            # Store date/time for file uniqueness
            current_dt = datetime.now().strftime('%m_%d_%Y_%H_%M_%S')
            # print("Current date time = " + current_dt)
            rawimagename = current_dt + '_' + rawimagename
            name_with_path = os.path.join(path, rawimagename + '.jpg')

            print('Name of raw image will be: ', rawimagename)
            # write the date in the column D
            sheet['D' + str(index+2)] = datetime.now().strftime('%m_%d_%Y')
            # write the time in the column E
            sheet['E' + str(index+2)] = datetime.now().strftime('%H_%M_%S')
            # change the color of the text in the column F
            sheet['F' + str(index+2)].font = Font(color=colors.RED)
            # save the path of the photo
            sheet['F' + str(index+2)].hyperlink = name_with_path
            print(name_with_path)

            # take picture
            func_TakeNikonPicture(name_with_path)
            time.sleep(1)
            swift.set_position(x=41.9246,
                               y=288.7331,
                               z=z,
                               speed=20, wait=True)

            move_to((row_pos, col_pos, z))  # bottom of the vial
            time.sleep(1)
            # step before to lift the vial : circular mouvement of the arm
            move_to((row_pos, col_pos, zc+20))
            time.sleep(1)
            move_to((row_pos, col_pos, zc+1))  # put the vial in his hall
            time.sleep(1)

            lift_Vial(xdep, ydep, (row_pos, col_pos, zc+1))  # lift the vial
            time.sleep(1)
            lift_Vial(xdep, ydep, (row_pos, col_pos, z))  # grow up
            time.sleep(1)

            swift.reset(wait=True)
            time.sleep(1)

    if df["Time between 2 global order (s)"].iloc[0]-number_vial*10 < 0:
        # the time that people want to wait between 2 photos is less then the time to take all of the last global order
        print('time to wait too short')
    else:
        time.sleep(df["Time between 2 global order (s)"].iloc[0] -
                   number_vial*10)    # 10s between 2 photos
        print(df["Time between 2 global order (s)"].iloc[0]-number_vial*10)

# setting width of column F to 80
sheet.column_dimensions['F'].width = float(75)

copy_name, ext = file_name.split(".")
excel.save(copy_name + '_out.xlsx')
