import os
import sys
import time

import keyboard
from uarm.wrapper import SwiftAPI

#sys.path.append(os.path.join(os.path.dirname(__file__), '../../..'))


swift = SwiftAPI(filters={'hwid': 'USB VID:PID=2341:0042'})#port="COM17"

device_info = swift.get_device_info()
print(device_info)
firmware_version = device_info['firmware_version']
if firmware_version.startswith(('0.', '1.', '2.', '3.')):
    speed = 50000
else:
    speed = 10
swift.set_mode(0)
swift.reset(speed=speed, wait=True)
time.sleep(2)
# set offset because not straight
swift.set_wrist(angle=100, wait=True)
effector = input("Enter end effector type:-> gripper OR pump OR holder: ")


def getInfo():
    print(swift.get_device_info())
    print(swift.get_power_status())
    print(swift.get_limit_switch())
    print(swift.get_gripper_catch())
    print(swift.get_pump_status())
    print(swift.get_mode())
    print(swift.get_servo_attach(servo_id=0))
    print(swift.get_servo_attach(servo_id=1))
    print(swift.get_servo_attach(servo_id=2))
    print(swift.get_servo_attach(servo_id=3))
    print(swift.get_servo_angle())
    print(swift.get_polar())
    print(swift.get_position())
    print(swift.get_analog(0))
    print(swift.get_digital(0))


def homePos():
    swift.set_position(x=180, y=0, z=75, speed=speed, wait=True)


def toggleGripper():
    state = swift.get_gripper_catch()
    if state == 0:
        swift.set_gripper(catch=True, check=True, wait=True)
        time.sleep(2)
    elif state == 1:
        swift.set_gripper(catch=False, check=True, wait=True)
        time.sleep(2)


def togglePump():
    state = swift.get_pump_status()
    if state == 0:
        swift.set_pump(on=True, check=True, wait=True)
        time.sleep(2)
    elif state == 1:
        swift.set_pump(on=False, check=True, wait=True)
        time.sleep(2)


def motorsUnlock():
    swift.set_servo_detach(0, wait=True)
    swift.set_servo_detach(1, wait=True)
    swift.set_servo_detach(2, wait=True)


def motorsLock():
    swift.set_servo_attach(0, wait=True)
    swift.set_servo_attach(1, wait=True)
    swift.set_servo_attach(2, wait=True)


def moveManually():
    while True:
        # move forward
        if keyboard.is_pressed('w'):
            swift.set_position(x=1, y=0, z=0, speed=speed,
                               wait=True, relative=True)
            # print(swift.get_position())
    # move backward
        elif keyboard.is_pressed('s'):
            swift.set_position(x=-1, y=0, z=0, speed=speed,
                               wait=True, relative=True)
            # print(swift.get_position())
    # move left
        elif keyboard.is_pressed('a'):
            swift.set_position(x=0, y=1, z=0, speed=speed,
                               wait=True, relative=True)
            # print(swift.get_position())
    # move right
        elif keyboard.is_pressed('d'):
            swift.set_position(x=0, y=-1, z=0, speed=speed,
                               wait=True, relative=True)
            # print(swift.get_position())
    # move up
        elif keyboard.is_pressed('up'):z
            swift.set_position(x=0, y=0, z=1, speed=speed,
                               wait=True, relative=True)
            # print(swift.get_position())
    # move down
        elif keyboard.is_pressed('down'):
            swift.set_position(x=0, y=0, z=-1, speed=speed,
                               wait=True, relative=True)
            # print(swift.get_position())
    # activate gripper/pump
        elif keyboard.is_pressed('space'):
            if effector == "gripper":  # if gripper is attached
                toggleGripper()
            elif effector == "pump":  # if pump is attached
                togglePump()
            elif effector == "holder":
                print("No action performed")
    # unlock all motors to manually position the arm
        elif keyboard.is_pressed('u'):
            motorsUnlock()
    # lock all motors to set its position
        elif keyboard.is_pressed('l'):
            motorsLock()
    # set the points and then break out of the while loop
        elif keyboard.is_pressed('return'):
            # set the end points of the pallet
            print(swift.get_position())
            return swift.get_position()
            break


def edgesPos():
    print("Define Bottom Right point")
    pt1 = moveManually()
    time.sleep(1)
    print("Define Top Right point")
    pt2 = moveManually()
    time.sleep(1)
    while True:
        verticalNumber = input("How many points in this direction:")
        if verticalNumber.isdigit():
            break
        else:
            pass
    time.sleep(1)
    print("Define Bottom Left point")
    pt3 = moveManually()
    time.sleep(1)
    while True:
        horizontalNumber = input("How many points in this direction:")
        if horizontalNumber.isdigit():
            break
        else:
            pass
    time.sleep(1)
    return pt1, pt2, pt3, verticalNumber, horizontalNumber


def disconnect():
    swift.reset(wait=True)
    time.sleep(1)
    # clear all the commands before disconnecting
    swift.flush_cmd(wait_stop=True)
    time.sleep(2)
    swift.disconnect()


def calculateDistance():
    pt1, pt2, pt3, verticalNumber, horizontalNumber = edgesPos()
    verticalDistance = (
        abs(float(pt1[0]) - float(pt2[0])) / int(verticalNumber))
    horizontalDistance = (
        abs(float(pt1[1]) - float(pt3[1])) / int(horizontalNumber))
    return verticalDistance, horizontalDistance


def setPickupLocation():
    print("Define Pickup Location")
    time.sleep(1)
    moveManually()
    time.sleep(1)
    print("Pick up location set")
    pick = swift.get_position()
    print(pick)
    return pick

def pickup():
    pick = setPickupLocation()
    swift.set_position(pick, wait=True)
    toggleGripper()
    time.sleep(1)


# def movement():
#     pt1, pt2, pt3, verticalNumber, horizontalNumber = edgesPos()
#     verticalDistance, horizontalDistance = calculateDistance()
#     pick = setPickupLocation()
#     homePos()
#     print("Stay back")
#     time.sleep(3)
#     # go to pick up location
#     pickup()
#     # go to first point
#     swift.set_position(pt1[0], pt1[1], pt1[2], speed=speed, wait=True)


def main():
    homePos()
    moveManually()
    moveManually()
    moveManually()
    moveManually()
    disconnect()


if __name__ == "__main__":
    main()
