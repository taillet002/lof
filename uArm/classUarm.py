import os
import sys
import time
import pandas as pd
import keyboard
from uarm.wrapper import SwiftAPI
import numpy as np

dictLetters2Num = {'A':0,'B':1,'C':2,'D':3,'E':4,'F':5,'G':6,'H':7,'I':8,'J':9,'K':10,'L':11,'M':12,'N':13,'O':14,'P':15,'Q':16,'R':17,'Q':18,'T':19,'U':20,'V':21,'W':22,'X':23,'Y':24,'Z':25,
    'a':0,'b':1,'c':2,'d':3,'e':4,'f':5,'g':6,'h':7,'i':8,'j':9,'k':10,'l':11,'m':12,'n':13,'o':14,'p':15,'q':16,'r':17,'s':18,'t':19,'u':20,'v':21,'w':22,'x':23,'y':24,'z':25}
    
class rack():
    
    def __init__(self,nCols,nRows,diameter,name='defaultName'):
        self.nCols = nCols
        self.nRows = nRows
        self.name = name
        self.referenceZUarm = 0        
        
        self.rackCoordinates = {bottom:0, top:0}
        self.vialHeight = 60
         
        self.bottomZPos = 0
        self.heightEffector = 10
        #spaceBound, 0: above bottom, 1: above Top, 2: below top to grip
        self.offSets = {bottom:'', }
        self.diameter = diameter
        #p1 -- p2
        #|     |
        #p4 -- p3
        self.refPoints = pd.DataFrame(index=['p1','p2','p3','p4'], columns=['x', 'y', 'z'])
        self.defineRefPos()
        
    def defineRefPos(self):
        self.wellZPos = {'gripOut': self.referenceZUarm+self.spaceBound[1]+2*self.heightEffector,'ground':self.referenceZUarm,'top':self.heightEffector+self.referenceZUarm+self.spaceBound[1],'gripIn':self.referenceZUarm+self.spaceBound[2]+self.heightEffector,'bottom':self.referenceZUarm+self.spaceBound[0]}
    
    def setCalibrationSimulation(self):
        self.refPoints.iloc[0] = [0,10,0]
        self.refPoints.iloc[1] = [10,10,0]
        self.refPoints.iloc[2] = [10,0,0]
        self.refPoints.iloc[3] = [0,0,0]

    def setPosition(self,uArm):
        i = 0
        list_instructions = ['set P1 point (top left)','set P2 point (top right)','set P3 point (bottom right)', 'set P4 point (bottom left)']
        while i < 4:
            posi = [0,0,0]
            time.sleep(1)
            True
#            while True:
            print(list_instructions[i])
            uArm.moveManually()
            posi = uArm.swift.get_position()
            self.refPoints.iloc[i] = posi  # ajout colonne au dataframe
            print('coord :',posi)
            i = i+1
        time.sleep(1)
        print('set Ground reference for Z axis')
        uArm.moveManually()
        posi = uArm.swift.get_position()
        self.referenceZUarm = posi[2]
        time.sleep(1)
        
        print('set Top reference for Z axis')
        uArm.moveManually()
        posi = uArm.swift.get_position()
        self.heightEffector = posi[2]-self.referenceZUarm
        time.sleep(1)
        self.setMappingPoints()
        uArm.swift.reset(speed=uArm.speed, wait=True)
        
    def setMappingPoints(self):
        # positions rack definition
        xs,  ys = np.meshgrid(np.linspace(0, 1, self.nCols), np.linspace(0, 1, self.nRows))

        # x corrections
        p1 = self.refPoints.loc['p1']
        p2 = self.refPoints.loc['p2']
        p3 = self.refPoints.loc['p3']
        p4 = self.refPoints.loc['p4']
        
        xs[0, :] = np.linspace(p1[0], p2[0], self.nCols)
        xs[-1, :] = np.linspace(p4[0], p3[0], self.nCols)
        # pour chaque colonne on reprend les écarts (sauf la 1 et 15)
        for c in range(xs.shape[1]-1):
            xs[:, c] = np.linspace(xs[0, c],xs[-1, c], self.nRows)
    
        # y corrections
        ys[0, :] = np.linspace(p1[1], p2[1], self.nCols)
        ys[-1, :] = np.linspace(p4[1], p3[1], self.nCols)
        for c in range(ys.shape[1]-1):
            ys[:, c] = np.linspace(ys[0, c], ys[-1, c], self.nRows)
    
        self.xs = xs
        self.ys = ys
    
        
        self.defineRefPos()
        
        
    def well(self,w):
        
        col = int(w[1:])-1
        row = dictLetters2Num[w[0]]
        return [self.xs[row,col],self.ys[row,col]]
            
class uArmRobot():
            
    def __init__(self,effector):
        self.swift = None
        self.device_info = None
        self.speed = 0
        self.device_info = None
        self.effector = effector
        
    def connect(self):
        self.swift = SwiftAPI(filters={'hwid': 'USB VID:PID=2341:0042'})#port="COM17"
        self.swift.waiting_ready(timeout=3) #waiting time to connect before timeout
        self.device_info = self.swift.get_device_info()
        print(self.device_info)
        firmware_version = self.device_info['firmware_version']
        if firmware_version.startswith(('0.', '1.', '2.', '3.')):
            self.speed = 50000
        else:
            self.speed = 10
        self.swift.set_mode(0) #mode normal (par défaut = 0, 1 = lser mode, 2 = 3D printing mode etc.
        self.swift.reset(speed=self.speed, wait=True)
        time.sleep(2)
        # set offset because not straight
        self.swift.set_wrist(angle=90, wait=True) #replie le bras à 90° ?

    def disconnect(self):
        
        self.swift.reset(wait=True)
        time.sleep(1)
        # clear all the commands before disconnecting
        self.swift.flush_cmd(wait_stop=True)
        time.sleep(2)
        self.swift.disconnect()
        
    def getInfo(self):
        
        if (self.swift.connected()):
            print(swift.get_device_info())
            print(swift.get_power_status())
            print(swift.get_limit_switch())
            print(swift.get_gripper_catch())
            print(swift.get_pump_status())
            print(swift.get_mode())
            print(swift.get_servo_attach(servo_id=0))
            print(swift.get_servo_attach(servo_id=1))
            print(swift.get_servo_attach(servo_id=2))
            print(swift.get_servo_attach(servo_id=3))
            print(swift.get_servo_angle())
            print(swift.get_polar())
            print(swift.get_position())
            print(swift.get_analog(0))
            print(swift.get_digital(0))
            #effector = input("Enter end effector type:-> gripper OR pump OR holder: ")
            
    def toggleGripper(self):
        state = self.swift.get_gripper_catch()
        #return self.swift.get_gripper_catch()
        if state == 0:
            self.swift.set_gripper(catch=True, check=True, wait=True)
            time.sleep(2)
        else:
            self.swift.set_gripper(catch=False, check=True, wait=True)
            time.sleep(2)
    
    def motorsUnlock(self):
        self.swift.set_servo_detach(0, wait=True)
        self.swift.set_servo_detach(1, wait=True)
        self.swift.set_servo_detach(2, wait=True)
    
    def motorsLock(self):
        self.swift.set_servo_attach(0, wait=True)
        self.swift.set_servo_attach(1, wait=True)
        self.swift.set_servo_attach(2, wait=True)

    def moveTo(self,rack,pos='A1',wellZPos='top'):
        #get pos
        originPos = self.swift.get_position()
        destPos = rack.well(pos)
        destPos.append(originPos[2])
        destPos[2] = rack.wellZPos[wellZPos]
        print('... uArm is moving to pos ',pos,' in rack ',rack.name,' coordinates :',destPos)
        self.swift.set_position(x=round(destPos[0],4), y=round(destPos[1],4),z=round(destPos[2],4), speed=self.speed,wait=True)
    def moveFrom(self,rack,wellZPos='top',accurate=False,stepsN=10): #Change Z Position
        originPos = self.swift.get_position()
        if (accurate):
            seqSteps = np.linspace(originPos[2],rack.wellZPos[wellZPos],stepsN)
        else: 
            seqSteps = [rack.wellZPos[wellZPos]]
        for c,zCoord in enumerate(seqSteps):
            originPos[2] = zCoord
            self.swift.set_position(x=round(originPos[0],4), y=round(originPos[1],4),z=round(originPos[2],4), speed=self.speed,wait=True)        

    def pickVial(self,rack,pos='A1'):
        #check if grip is open or not
        if (self.swift.get_gripper_catch()==0):
            self.moveFrom(rack,wellZPos='top')
            #open grip
            self.moveTo(rack,pos=pos,wellZPos='top')
            self.moveFrom(rack,wellZPos='gripIn',accurate=True)
            uArm1.toggleGripper()
            self.moveFrom(rack,wellZPos='top',accurate=True)
            self.moveFrom(rack,wellZPos='gripOut',accurate=True)
            
            self.moveTo(rack,pos=pos,wellZPos='gripOut')

        else:
            print('Gripper is already in use')
    def dropVial(self,rack,pos='A1'):
        if (self.swift.get_gripper_catch()!=0):
            self.moveFrom(rack,wellZPos='gripOut',accurate=True)
            #open grip
            self.moveTo(rack,pos=pos,wellZPos='gripOut')
            self.moveFrom(rack,wellZPos='top',accurate=True)
            self.moveFrom(rack,wellZPos='gripIn',accurate=True)
            uArm1.toggleGripper()
            self.moveFrom(rack,wellZPos='top')        
        else:
            print('Nothing to drop as Gripper is opened')
                    

    
    def moveManually(self):
        while True:
            # move forward
            if keyboard.is_pressed('w'):
                self.swift.set_position(x=1, y=0, z=0, speed=self.speed,
                                wait=True, relative=True)

            # move backward
            elif keyboard.is_pressed('s'):
                self.swift.set_position(x=-1, y=0, z=0, speed=self.speed,
                                wait=True, relative=True)
            
            # move right
            elif keyboard.is_pressed('a'):
                self.swift.set_position(x=0, y=1, z=0, speed=self.speed,
                                wait=True, relative=True)
            
            # move left
            elif keyboard.is_pressed('d'):
                self.swift.set_position(x=0, y=-1, z=0, speed=self.speed,
                                wait=True, relative=True)
                
            # move up
            elif keyboard.is_pressed('up'):
                self.swift.set_position(x=0, y=0, z=1, speed=self.speed,
                                wait=True, relative=True)

            # move down
            elif keyboard.is_pressed('down'):
                self.swift.set_position(x=0, y=0, z=-1, speed=self.speed,
                                wait=True, relative=True)

            # activate gripper/pump
            elif keyboard.is_pressed('space'):
                if self.effector == "gripper":  # if gripper is attached
                    self.toggleGripper()
                elif self.effector == "pump":  # if pump is attached
                    togglePump()
                elif self.effector == "holder":
                    print("No action performed")
            # unlock all motors to manually position the arm
            elif keyboard.is_pressed('u'):
                self.motorsUnlock()
            # lock all motors to set its position
            elif keyboard.is_pressed('l'):
                self.motorsLock()
            # set the points and then break out of the while loop
            elif keyboard.is_pressed('return'):
                # set the end points of the pallet
                # return swift.get_position()
                break
        
    
if __name__ == "__main__":
    uArm1 = uArmRobot(effector = 'gripper')
    rack1 = rack(nCols=10,nRows=3,diameter=14,name='rack1')
    uArm1.connect()
    input("Press keyboard to continue")
    rack1.setPosition(uArm1)
    rack1.setMappingPoints()
    #rack1.setCalibrationSimulation()
    
    #input("Press keyboard to quit")
    #uArm1.toggleGripper()
    #input("Press keyboard to quit")
    #uArm1.disconnect()