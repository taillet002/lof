# -*- coding: utf-8 -*-
"""loss function definitions according to publications

"""
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import warnings

from keras import backend as K
from keras.objectives import categorical_crossentropy
from keras.layers import Flatten

if K.image_dim_ordering() == 'tf':
    import tensorflow as tf

epsilon = 1e-4

def rpn_loss_regr(num_anchors):
    def rpn_loss_regr_fixed_num(y_true, y_pred):
        x = y_true[:, :, :, 4 * num_anchors:2*4 * num_anchors] - y_pred
        x_abs = K.abs(x)
        x_bool = K.cast(K.less_equal(x_abs, 1.0), tf.float32)
        return K.sum(y_true[:, :, :, :4 * num_anchors] * (x_bool * (0.5 * x * x) + (1 - x_bool) * (x_abs - 0.5))) / K.sum(epsilon + y_true[:, :, :, :4 * num_anchors])

    return rpn_loss_regr_fixed_num


def rpn_loss_cls(num_anchors):
    def rpn_loss_cls_fixed_num(y_true, y_pred):
        return K.sum(y_true[:, :, :, :num_anchors] * K.binary_crossentropy(y_true[:, :, :,num_anchors:],y_pred[:, :, :, :])) / K.sum(epsilon + y_true[:, :, :, :num_anchors])

    return rpn_loss_cls_fixed_num

def smooth_l1(y_true, y_pred):
    sigma=3.0
    sigma_squared = sigma ** 2
    
    """ Compute the smooth L1 loss of y_pred w.r.t. y_true.
    Args
        y_true: Tensor from the generator of shape (B, N, 5). The last value for each box is the state of the anchor (ignore, negative, positive).
        y_pred: Tensor from the network of shape (B, N, 4).
    Returns
        The smooth L1 loss of y_pred w.r.t. y_true.
    """
    # separate target and state
    regression        = y_pred
    regression_target = y_true[:, :, :4]
    anchor_state      = y_true[:, :, 4]

    # filter out "ignore" anchors
    indices = tf.where(K.equal(anchor_state, 1))
    regression = tf.gather_nd(regression, indices)
    regression_target = tf.gather_nd(regression_target, indices)

    # compute smooth L1 loss
    # f(x) = 0.5 * (sigma * x)^2          if |x| < 1 / sigma / sigma
    #        |x| - 0.5 / sigma / sigma    otherwise
    regression_diff = regression - regression_target
    regression_diff = K.abs(regression_diff)
    regression_loss = tf.where(
        K.less(regression_diff, 1.0 / sigma_squared),
        0.5 * sigma_squared * K.pow(regression_diff, 2),
        regression_diff - 0.5 / sigma_squared)

    # compute the normalizer: the number of positive anchors
    normalizer = K.maximum(1, K.shape(indices)[0])
    normalizer = K.cast(normalizer, dtype=K.floatx())
    return K.sum(regression_loss) / normalizer

def class_loss_cls(y_true, y_pred):
    #y[0, :, :]
    #return K.shape(y_pred)[1] 
    #shape = y_true.get_shape().as_list()
    #Y = tf.reshape(y_true, (shape[0].value, shape[1].value*shape[2].value))
    #Y = tf.reshape(y_true, tf.TensorShape([-1, 1]))
    #Y = tf.TensorShape([shape[0],shape[1]*shape[2]])
    #Y = Flatten()(y_true)
    
    return K.mean(categorical_crossentropy(y_true, y_pred))
    