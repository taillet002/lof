# coding=utf-8
import cv2 #Traitement des images
import selectivesearch #ROI proposals based on selective search (cf. article)
import tensorflow as tf #outil de google pour les RN
import numpy as np #permet de gérer des matrices à la matlab
from os import listdir #gestion repertoire et fichier
from os.path import splitext, isdir, isfile, join #gestion repertoire et fichier
import matplotlib.pyplot as plt #équivalent de plot matlab

def rect_intersec(rect1_coord,rect2_coord):
    G1 = rect1_coord[0]
    G2 = rect2_coord[0]
    D1 = rect1_coord[0]+rect1_coord[2]
    D2 = rect2_coord[0]+rect2_coord[2]
    
    H1 = rect1_coord[1]
    H2 = rect2_coord[1]
    B1 = rect1_coord[1]+rect1_coord[3]
    B2 = rect2_coord[1]+rect2_coord[3]
    t = 0
    #test sur les x
    if ((G1 > G2) != (G1>D2)):
        t = 1
        return t 
        
    if ((G2 > G1) != (G2>D1)):
        t = 1
        return t
    
    if ((H2 > H1) != (B2>H1)):
        t = 1
        return t 
        
    if ((H1 > H2) != (B1>H2)):
        t = 1
        return t
    return t
    
def get_top_k(self, boxes, scores, k):
    a = np.argsort(np.squeeze(scores))[::-1]
    return boxes[a[:k]]

# by Ross Girshick
def non_maximum_supression(dets, thresh):
    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]
    scores = dets[:, 4]

    #areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    areas = (x2 - x1) * (y2 - y1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1)
        h = np.maximum(0.0, yy2 - yy1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter+ 1e-6)

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]
    return keep
    
def boxNormalisation2ROIPool(box,shape):
    temp = []
    box = np.copy(box)
    box = np.float32(box)
    W = [shape[0],shape[1],shape[0],shape[1]]
    for b in box:
        b1 = [i1/(W[c]-1) for c,i1 in enumerate(b)]
        temp.append([b1[1],b1[0],b1[3],b1[2]])
    
    return np.array(temp,dtype=np.float32)

def boxNormalisation2Reg(boxes,shape):
        boxes_ = np.copy(boxes)
        boxes_ = np.float32(boxes_)
        dw = 1./shape[1]
        dh = 1./shape[0]
        x = (boxes_[:, 0] + boxes_[:, 2])*dw/2.
        y = (boxes_[:, 1] + boxes_[:, 3])*dh/2.
        w = (boxes_[:, 2] - boxes_[:, 0])*dw
        h = (boxes_[:, 3] - boxes_[:, 1])*dh
        x = np.reshape(x, (-1, 1))
        y = np.reshape(y, (-1, 1))
        w = np.reshape(w, (-1, 1))
        h = np.reshape(h, (-1, 1))
        return np.hstack([x, y, w, h])
        
def invBoxNormalisation2Reg(boxes,shape):
        dx = shape[1]
        dy = shape[0]

        x = boxes[:, 0]*dx
        y = boxes[:, 1]*dy
        w = boxes[:, 2]*dx
        h = boxes[:, 3]*dy
        x -= w/2.
        y -= h/2.
        x2 = x + w
        y2 = y + h
        x = np.reshape(x, (-1, 1))
        y = np.reshape(y, (-1, 1))
        x2 = np.reshape(x2, (-1, 1))
        y2 = np.reshape(y2, (-1, 1))
        return np.hstack([x, y, x2, y2])
        
def coordParametrization(boxes1,boxes2):
        x = np.divide((boxes1[:, 0] - boxes2[:,0]),boxes2[:,2])
        
        y = np.divide((boxes1[:, 1] - boxes2[:,1]),boxes2[:,3])
        w = np.log(np.divide(boxes1[:,2],boxes2[:,2]))
        h = np.log(np.divide(boxes1[:,3],boxes2[:,3]))
        x = np.reshape(x, (-1, 1))
        y = np.reshape(y, (-1, 1))
        w = np.reshape(w, (-1, 1))
        h = np.reshape(h, (-1, 1))
        return np.hstack([x, y, w, h])

def invCoordParametrization(t,boxes2):
        x = np.multiply(t[:, 0],boxes2[:,2])+boxes2[:,0]
        y = np.multiply(t[:, 1],boxes2[:,3])+boxes2[:,1]
        w = np.multiply(np.exp(t[:, 2]),boxes2[:,2])
        h = np.multiply(np.exp(t[:, 3]),boxes2[:,3])
        x = np.reshape(x, (-1, 1))
        y = np.reshape(y, (-1, 1))
        w = np.reshape(w, (-1, 1))
        h = np.reshape(h, (-1, 1))
        return np.hstack([x, y, w, h])

def display(figurename='Default',typeData='',img=np.array([]),unpackSize=(100,100),imResize=(100,100),boxes=[],list_class=[],graph=[1,1]):
    def Monitoring():
        a = 1
        #f,ax = plt.subplots(1,2,sharex=True,sharey=True)
        
        #ax = ax.flatten()
        #ax[0].clear()
        #ax[0].plot(graph,'k*',label='Train set')
        #ax[0].set_xlabel('N Epoch')
        plt.figure(figurename)
        plt.plot(graph,'k*',label='Train set')
        plt.show()
        plt.pause(0.003)
        
    def ImgWithBox():
        Win_H = 800
        Win_W = 800
        Nx = int(Win_W/imResize[1])
        Ny = int(Win_H/imResize[0])
        margin = 2
        strideX = imResize[1]+margin
        strideY = imResize[0]+margin
        imgPosX = np.array([i*(strideX) for i in range(0,Nx)],dtype=np.uint16)
        imgPosY = np.array([i*(strideY) for i in range(0,Ny)],dtype=np.uint16)
        ImgFrame = 0*np.ones((strideY*Ny,strideX*Nx,3),dtype=np.uint8)
        count2 = 0
        for count in range(0,min(Nx*Ny,img.shape[0])):

            img1 = cv2.cvtColor(img[count,:].reshape(unpackSize), cv2.COLOR_GRAY2BGR)
            color_class = [(0,0,255),(0,255,0),(255,0,0)]
            iX_rebuildImg = count%Nx
            if (iX_rebuildImg == 0):
                iY_rebuildImg = count2
                count2 = count2+1
            cX_rebuildImg = imgPosX[iX_rebuildImg]
            cY_rebuildImg =imgPosY[iY_rebuildImg]
            if (count < len(boxes)):
                for count1,i in enumerate(boxes[count]):
                    #numC,_ = self.dict_ClassObj[list_class[count]]
                    numC = 1
                    i = np.array(i,dtype=np.int32)
                    if (len(list_class)>0):
                        numC = list_class[count]
                        numC = numC[count1]
                        if (numC>0):
                            x1 = i[0]
                            y1 = i[1]
                            x2 = i[2]
                            y2 = i[3]
                            img1 = cv2.rectangle(img1, (x1,y1), (x2,y2), color_class[numC], 2)
                    else:
                        x1 = i[0]
                        y1 = i[1]
                        x2 = i[2]
                        y2 = i[3]
                        img1 = cv2.rectangle(img1, (x1,y1), (x2,y2), color_class[numC], 2)
            img2 = cv2.resize(img1, imResize)
            ImgFrame[cY_rebuildImg:cY_rebuildImg+imResize[1],cX_rebuildImg:cX_rebuildImg+imResize[0],:] = img2
        return ImgFrame
        
    _dictSC = {'ImgWithBox': ImgWithBox,'Monitoring': Monitoring}
    _func = _dictSC.get(typeData,0)
    if (_func == 0):
        return 'error'
    else: 
        return _func()
def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    X = [0,0,0,0]
    iou = []
    for c,box in enumerate(boxA):
        X[0] = max(box[0], boxB[0]) #xA
        X[1] = max(box[1], boxB[1]) #yA
        X[2] = min(box[2], boxB[2]) #xB
        X[3] = min(box[3], boxB[3]) #yB

        # compute the area of intersection rectangle
        interArea = max(0, X[2] - X[0]) * max(0, X[3] - X[1])
 
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[c][2] - boxA[c][0]) * (boxA[c][3] - boxA[c][1])
        boxBArea = (boxB[2] - boxB[0])* (boxB[3] - boxB[1])

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        denominateur = float(boxAArea + boxBArea - interArea+ 1e-6)
        
        iou.append(interArea / denominateur)

    # return the intersection over union value
    return iou

def ROI_proposal(img):
    # perform selective search from https://github.com/AlpacaDB/selectivesearch
    h_,w_,_ = img.shape
    max = (h_-1)*(w_-1)
    img_lbl, regions = selectivesearch.selective_search(img, scale=200, sigma=0.9, min_size=10)
    candidates = []
    candidates_origin = []
    for r in regions:
        # excluding same rectangle (with different segments)
        x, y, w, h = r['rect']
        if [x,y,w,h] in candidates_origin:
            continue
        # excluding regions smaller than 200 pixels
        if r['size'] < 400:
            continue
        # suppresss big rectangle
        if (w*h) >= max:
            continue
        # distorted rects
        
        #if w / h > 1.5 or h / w > 1.5:
            #continue
        candidates_origin.append([x,y,w,h])
        candidates.append([x,y,x+w,y+h])	
    return candidates
    
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '')
    # Print New Line on Complete
    if iteration == total: 
        print()