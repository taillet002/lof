###########################################
# Definition de setFlow qui génère les matrices pour forward le RN
# Ce code se trouve normalement dans le fichier ClassDef.py dans le dossier lof\Pellets_Detection\Utils\, dans la classe DataFlow
# -*- coding: utf-8 -*-
###########################################

def setFlowRPN(df,outType,Config,splitData=0):

  # On récupe le nombre d'anchor et une liste avec les caractéristiques des anchors
  anchor_sizes = Config.anchor_box_scales
  anchor_ratios = Config.anchor_box_ratios
  num_anchors = len(anchor_sizes)*len(anchor_ratios)
  n_anchratios = len(anchor_ratios)
  
  # On resize les true box sur l'image de sortie du RN
  inputH,inputW = df.unpackSize[0],df.unpackSize[1]
  outputH,outputW = get_img_output_length(df.unpackSize)

  downscale = get_DownScale()
  
  #On préalloue les principales matrices
  y_rpn_overlap = np.zeros((df.inputImg.shape[0], outputH, outputW, num_anchors))
  y_is_box_valid = np.zeros((df.inputImg.shape[0], outputH, outputW, num_anchors))
  y_rpn_regr = np.zeros((df.inputImg.shape[0], outputH, outputW, num_anchors * 4))
  y_rpn_regrEncoder = np.zeros((df.inputImg.shape[0], outputH, outputW, num_anchors * 4))

  #On génère les boxAnchors de l'image    
  boxAnchors = []
  boxAnchorsIdx = []

  for anchor_size_idx in range(len(anchor_sizes)):
      for anchor_ratio_idx in range(n_anchratios):
        nAnchor = anchor_size_idx*n_anchratios+anchor_ratio_idx
        # Obtention de la largeur et hauteur de l'anchor
        anchor_x = anchor_sizes[anchor_size_idx] * anchor_ratios[anchor_ratio_idx][0]
        anchor_y = anchor_sizes[anchor_size_idx] * anchor_ratios[anchor_ratio_idx][1]	

        for ix in range(outputW):					

          # x-coordinates de l'anchor en cours
          x1_anc = downscale * (ix + 0.5) - anchor_x / 2
          x2_anc = downscale * (ix + 0.5) + anchor_x / 2	

          # on ignore les box en dehors de l'image					
          if x1_anc < 0 or x2_anc > inputW:
            continue
            
          for jy in range(outputH):
            # y-coordinates of the current anchor box
            y1_anc = downscale * (jy + 0.5) - anchor_y / 2
            y2_anc = downscale * (jy + 0.5) + anchor_y / 2

            # ignore boxes that go across image boundaries
            if y1_anc < 0 or y2_anc > inputH:
              continue
            
            anchorCoord = np.array([x1_anc, y1_anc, x2_anc, y2_anc]).reshape(1,4) #Array des coords au format 1,4 pour encoder
            y_rpn_regrEncoder[:,jy,ix,4*nAnchor:(4*nAnchor)+4] = boxNormalisation2Reg(anchorCoord,(1,1))[0,:]
            
            boxAnchors.append([x1_anc, y1_anc, x2_anc, y2_anc]) # List des coordonnées
            boxAnchorsIdx.append([ix,jy,nAnchor]) # List des anchors avec le numero de l'anchor
            
  boxAnchorsIdx = np.array(boxAnchorsIdx)
  boxAnchors = np.array(boxAnchors)

  if (outType == 'fit'):
    ######### Pour l'apprentissage seulement.
    #On recupère le nb de classe
    nb_classes = len(df.Database.pick_obj)+1
    # On convertit les classes de chaque box, de str vers integer
    inputClass = df.strClass_2_NumClass(df.inputClass)
    # On met dans une liste les True boxs (créée un array de list)
    boxTrue = np.copy(df.inputBox)
    for c,img in enumerate(df.inputImg):       
      #bbox_type = 'neg'
      # On parcourt les trues boxes et on check le IOU
      for c1,boxlabel in enumerate(df.inputBox[c]): 
        IoU = bb_intersection_over_union(boxAnchors, boxlabel) 
        IoU = np.array(IoU)
        # Calcul des coordonnées

        # Classification
        # les Négatifs (background)
        Idx = np.where(IoU < C.rpn_min_overlap)[0] #Bckgnd (Negative)
        if Idx.size>0:
          minVal = IoU[Idx]
          temp = boxAnchorsIdx[Idx]
          IdxW = temp[:,0] 
          IdxH = temp[:,1]
          y_rpn_overlap[c,IdxH,IdxW,temp[:,2]] = 0 
          y_is_box_valid[c,IdxH,IdxW,temp[:,2]] = 1

        # Les neutres (on sait pas trop)
        Idx = np.where((C.rpn_min_overlap < IoU) & (IoU < C.rpn_max_overlap))[0] #Neutral
        if Idx.size > 0:
          neutralVal = IoU[Idx]
          temp = boxAnchorsIdx[Idx] 

          IdxW = temp[:,0]
          IdxH = temp[:,1]
          y_rpn_overlap[c,IdxH,IdxW,temp[:,2]] = 0 
          y_is_box_valid[c,IdxH,IdxW,temp[:,2]] = 0
          
        # Les positifs
        Idx = np.where(IoU > C.rpn_max_overlap)[0] #Positive
        boxlabel = np.array(boxlabel).reshape(1,4)
        nbAnchors2add = 0
        if Idx.size > 0:

          #Check si les valeurs dans y_rpn_overlap sont plus faibles que celles qu'on veut ajouter. Sinon, on laisse l'ancienne
          for Idx_ in Idx:
            temp = boxAnchorsIdx[Idx_]
            IdxW = temp[0] 
            IdxH = temp[1]
            IoU_ = IoU[Idx_]
            if (IoU_ > y_rpn_overlap[c,IdxH,IdxW,temp[2]]):
              nbAnchors2add = nbAnchors2add+1
              y_rpn_overlap[c,IdxH,IdxW,temp[2]] = IoU_
              y_is_box_valid[c,IdxH,IdxW,temp[2]] = 1
              start = np.array(4 * temp[2],dtype=np.int32)
              anchorCoord = y_rpn_regrEncoder[c,IdxH,IdxW,start:start+4].reshape((1,4))
              coordTrueBox = boxNormalisation2Reg(boxlabel,(1,1))
              #coordAnchorBox = boxNormalisation2Reg(anchorCoord,(1,1))
              coordParam = coordParametrization(coordTrueBox,anchorCoord).reshape((4))
              y_rpn_regr[c,IdxH,IdxW,start:start+4] = coordParam        
              #print(coordParam,anchorCoord)
              
        if (nbAnchors2add == 0): #Si on en trouve pas de valuer > 0.7, on met l'anchor avec le meilleur IoU (on remplace)
          Idx = np.argmax(IoU)
          temp = boxAnchorsIdx[Idx]
          IdxW = temp[0] 
          IdxH = temp[1]
          IoU_ = IoU[Idx]
          y_rpn_overlap[c,IdxH,IdxW,temp[2]] = 1
          y_is_box_valid[c,IdxH,IdxW,temp[2]] = 1
          start = np.array(4 * temp[2],dtype=np.int32)
          anchorCoord = y_rpn_regrEncoder[c,IdxH,IdxW,start:start+4].reshape((1,4))
          coordTrueBox = boxNormalisation2Reg(boxlabel,(1,1))
          #coordAnchorBox = boxNormalisation2Reg(anchorCoord,(1,1))
          coordParam = coordParametrization(coordTrueBox,anchorCoord).reshape((4))        
          y_rpn_regr[c,IdxH,IdxW,start:start+4] = coordParam


      # S'il y a trop de box négatives, cela peut poser un probleme....
      # On désactive des box négatives 
      pos_locs = np.where(np.logical_and(y_rpn_overlap[c,:,:,:] > C.rpn_max_overlap,y_is_box_valid[c,:,:,:] == 1))
      neg_locs = np.where(np.logical_and(y_rpn_overlap[c,:,:,:] == 0,y_is_box_valid[c,:,:,:] == 1))
      y_rpn_overlap[c, pos_locs[0][:], pos_locs[1][:], pos_locs[2][:]] = 1 
      num_pos = len(pos_locs[0])
      #print(num_pos,y_rpn_overlap[c, pos_locs[0][:], pos_locs[1][:], pos_locs[2][:]])
      num_regions = 256
      if len(pos_locs[0]) > num_regions/2:
        shuffleList = np.array(range(len(pos_locs[0])),dtype=np.int32)
        np.random.shuffle(shuffleList)
        val_locs = shuffleList[0:len(pos_locs[0]) - num_regions/2]
        y_is_box_valid[c, pos_locs[0][val_locs], pos_locs[1][val_locs], pos_locs[2][val_locs]] = 0
        num_pos = num_regions/2

      if len(neg_locs[0]) + num_pos > num_regions:
        shuffleList = np.array(range(len(neg_locs[0])),dtype=np.int32)
        np.random.shuffle(shuffleList)
        val_locs = shuffleList[0:num_regions - num_pos]
        y_is_box_valid[c, neg_locs[0][val_locs], neg_locs[1][val_locs], neg_locs[2][val_locs]] = 0
    
    pos_locs = np.where(np.logical_and(y_rpn_overlap[c,:,:,:] > C.rpn_max_overlap,y_is_box_valid[c,:,:,:] == 1))
    neg_locs = np.where(np.logical_and(y_rpn_overlap[c,:,:,:] == 0,y_is_box_valid[c,:,:,:] == 1))
    print(len(pos_locs),len(neg_locs))
    y_rpn_cls = np.concatenate([y_is_box_valid, y_rpn_overlap], axis=3)
    y_rpn_regr = np.concatenate([np.repeat(y_rpn_overlap,4,axis=3),y_rpn_regr,y_rpn_regrEncoder],axis=3)
    #On construit le tenseur global des images en normalisant les images de 0 à 1
    imgInput = df.inputImg.astype('float32')/255 
    imgInput = np.reshape(imgInput,(imgInput.shape[0],df.unpackSize[0],df.unpackSize[1],1))  
    lossInput = [y_rpn_cls, y_rpn_regr]

    return imgInput,lossInput
  
  else:
    #On génère l'encoder pour la paramétrisation pour chaque box et l'img a envoyer
    imgInput = df.inputImg.astype('float32')/255 
    imgInput = np.reshape(imgInput,(imgInput.shape[0],df.unpackSize[0],df.unpackSize[1],1))  
    return imgInput, y_rpn_regrEncoder
  
C = Config()