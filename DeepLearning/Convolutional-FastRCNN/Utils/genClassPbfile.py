import argparse



parser = argparse.ArgumentParser()
parser.add_argument('--class_list', dest='trainable_classes', required=True)
parser.add_argument('--outputPath', dest='label_map_path', required=True)

if __name__ == '__main__':
    args = parser.parse_args()
    trainable_classes_file = args.trainable_classes
    label_map_path = args.label_map_path
    save_label_map(label_map_path, trainable_classes_file)