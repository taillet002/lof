from keras.engine.topology import Layer
import keras.backend as K
if K.backend() == 'tensorflow':
    import tensorflow as tf

class RoiPoolingConv(Layer):
    '''ROI pooling layer for 2D inputs.
    See Spatial Pyramid Pooling in Deep Convolutional Networks for Visual Recognition,
    K. He, X. Zhang, S. Ren, J. Sun
    # Arguments
        pool_size: int
            Size of pooling region to use. pool_size = 7 will result in a 7x7 region.
        num_rois: number of regions of interest to be used
    # Input shape
        list of two 4D tensors [X_img,X_roi] with shape:
        X_img:
        `(1, channels, rows, cols)` if dim_ordering='th'
        or 4D tensor with shape:
        `(1, rows, cols, channels)` if dim_ordering='tf'.
        X_roi:
        `(1,num_rois,4)` list of rois, with ordering (x,y,w,h)
    # Output shape
        3D tensor with shape:
        `(1, num_rois, channels, pool_size, pool_size)`
    '''
    def __init__(self, pool_size, num_rois, Nbatch, **kwargs):

        self.dim_ordering = K.image_dim_ordering()
        assert self.dim_ordering in {'tf', 'th'}, 'dim_ordering must be in {tf, th}'

        self.pool_size = pool_size
        self.num_rois = num_rois
        self.nb_batch = Nbatch    
        super(RoiPoolingConv, self).__init__(**kwargs)

    def build(self, input_shape):
        if self.dim_ordering == 'th':
            self.nb_channels = input_shape[0][1]
        elif self.dim_ordering == 'tf':
            self.nb_channels = input_shape[0][3]
        

    def compute_output_shape(self, input_shape):
        if self.dim_ordering == 'th':
            return None, self.num_rois, self.nb_channels, self.pool_size, self.pool_size
        else:
            return None, self.num_rois, self.pool_size, self.pool_size, self.nb_channels

    def call(self, x, mask=None):

        assert(len(x) == 2)

        img = x[0]
        rois = x[1]

        input_shape = K.shape(img)
        
        h =  K.cast(input_shape[1], 'float32')
        w =  K.cast(input_shape[2], 'float32')
        
        
        final_output = []
        #for batchNum in range(self.nb_batch):
        outputs = []
        #tbatchNum = tf.constant(batchNum)
        for roi_idx in range(self.num_rois):

            #x1 = rois[batchNum, roi_idx, 0]*h
            #y1 = rois[batchNum, roi_idx, 1]*w
            #x2 = rois[batchNum, roi_idx, 2]*h
            #y2 = rois[batchNum, roi_idx, 3]*w
            
            x1 = rois[batchNum, roi_idx, 0]
            y1 = rois[batchNum, roi_idx, 1]
            x2 = rois[batchNum, roi_idx, 2]
            y2 = rois[batchNum, roi_idx, 3]
            #x = rois[0, roi_idx, 0]
            # y = rois[0, roi_idx, 1]
            #  w = rois[0, roi_idx, 2]
            #   h = rois[0, roi_idx, 3]
            
            #row_length = w / float(self.pool_size)
            #col_length = h / float(self.pool_size)

            num_pool_regions = self.pool_size

            x1 = K.cast(x1, 'int32')
            y1 = K.cast(y1, 'int32')
            x2 = K.cast(x2, 'int32')
            y2 = K.cast(y2, 'int32')
            
            #x = K.cast(x, 'int32')
            #y = K.cast(y, 'int32')
            #w = K.cast(w, 'int32')
            #h = K.cast(h, 'int32')
            
            #print(roi_idx,img[:, y1:y2, x1:x2, :],self.pool_size)
            #rs = tf.image.resize_images(img[:, y:y+h, x:x+w, :], (self.pool_size, self.pool_size))
            
            #rs = tf.image.resize_images(img[:, y1:y2, x1:x2, :], (self.pool_size, self.pool_size))
            rs = tf.image.crop_and_resize(img,rois,self.anchorBoxId,crop_size=(7,7),name='layer4') #return [num_boxes, crop_height, crop_width, depth].

            outputs.append(rs)

        outputs = K.concatenate(outputs, axis=0)
        
            #final_output.append(K.reshape(outputs, (1, self.num_rois, self.pool_size, self.pool_size, self.nb_channels)))
        
        #final_output = K.concatenate(final_output,axis=0)
        # self.dim_ordering == 'th':
        final_output = K.permute_dimensions(final_output, (1, 0, 2, 3, 4))
        #else:
        #    final_output = K.permute_dimensions(final_output, (0, 1, 2, 3, 4))
        return final_output
    
    def get_config(self):
        config = {'pool_size': self.pool_size,
                  'num_rois': self.num_rois}
        base_config = super(RoiPoolingConv, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
