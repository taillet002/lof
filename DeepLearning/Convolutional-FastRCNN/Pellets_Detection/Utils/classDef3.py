# coding=utf-8
from os.path import splitext, isdir, isfile, join #gestion repertoire et fichier
from os import listdir #gestion repertoire et fichier
import cv2 #Traitement des images
from XML2gen import *
from functions_support import *
import numpy as np

class ObjDef:
  def __init__(C,name,path_name):
      C.name = name
      C.path_name = path_name
      C.Nfiles = 0
      C.list_files = []
      C.set_listFiles()

  #Cette fonction permet de r�cup�rer la liste des fichiers dans le dossier
  def set_listFiles(C):
      #lister l'ensemble des images dans le dossier train
      tempfiles = []
      temprep = C.path_name
      if (isdir(temprep)):
          tempfiles = [f for f in listdir(temprep) if isfile(join(temprep,f))]
      C.list_files = tempfiles
      C.Nfiles = len(tempfiles)
  #Cette fonction permet de retourner une matrice du fichier image associ� dans trainFiles
  def get_img(C,num):

      if (isfile(join(C.path_name,C.list_files[num]))):
          temp = cv2.imread(join(C.path_name,C.list_files[num]),cv2.IMREAD_UNCHANGED)
          if (temp is not None):
              w0 = temp.shape[0]
              h0 = temp.shape[1]
              return temp,w0,h0
          else:
              return 0,0,0
      else:
          return 0,0,0
          
#Classe des base de données 
class DbDef:
    def __init__(C1,name,pick_obj,classDef={},imgSize=(400,400)):
        C1.name = name
        C1.pick_obj = pick_obj
        C1.imgSize = imgSize
        C1.classDef = classDef
        C1.path_name = None
        C1.labels = []
        C1.list_files = []
        C1.list_XML_files = []
        
    def read_path(C1,path_name):
        list_files = []
        C1.path_name = path_name
        path_nameImg = join(path_name,'img')
        for f in listdir(path_nameImg):
            file = join(path_nameImg,f)
            if isfile(file):
                name, ext = splitext(join(path_nameImg,f))
                if (ext == '.png'): 
                    list_files.append(f[:-4])
        C1.list_files,C1.Nfiles = list_files,len(list_files);

    #Cette fonction permet de retourner une matrice du fichier image associé dans trainFiles
    def get_img(C1,num,resize):
        path_nameImg = join(C1.path_name,'img')
        imgStack = 255*np.ones((len(num),resize[0]*resize[1]),dtype=np.uint8)
        for count,i in enumerate(num):
            temp = cv2.imread(join(path_nameImg,C1.list_files[i]+'.png'),cv2.IMREAD_UNCHANGED)
            temp = cv2.resize(temp, resize).reshape(resize[0]*resize[1])
            imgStack[count,:] = temp
        return imgStack

    #Cette fonction permet de retourner une matrice du fichier image associé dans trainFiles
    def get_labels(C1,num,scale=1):
      ROIStack = []
      classMat = []
      path_nameLabels = join(C1.path_name,'labels')
      temp = []
      for count,i in enumerate(num):
        VOCread = PascalVocReader(join(path_nameLabels,C1.list_files[i]+'.xml'))
        temp = VOCread.getShapes()
        subROIStack = []
        subclassMat = []
        for count,i1 in enumerate(temp):
          subclassMat.append(i1[0])
        # [labbel, [(x1,y1), (x2,y2), (x3,y3), (x4,y4)], color, color, difficult]
          temp = [i1[1][0][0],i1[1][0][1],i1[1][2][0],i1[1][2][1]]
          temp_ = [np.round(i/scale) for i in temp]
          subROIStack.append(temp_)  
        
        ROIStack.append(subROIStack)
        classMat.append(subclassMat)
      return ROIStack,classMat
      
    def gen_img(C1,path_name,class_ratios,pellets_density,Nimg,shuffle=True,showBckg=False):
        def gen_boxCoord(w,h,imgSize):
            xMax = imgSize[1] - w-1;
            yMax = imgSize[0] - h-1;
            c1 = [i3 for i3 in range(0,xMax)]
            c2 = [i3 for i3 in range(0,yMax)] 
            x1 = np.random.choice(c1)   
            y1 = np.random.choice(c2)                
            return x1,y1
        C1.path_name = path_name
        C1.class_ratios = class_ratios
        C1.pellets_density = pellets_density-1

        #On suppose le dossier existant

        #On compte le nombre de fichiers déjà présent
        C1.read_path(C1.path_name)
        #tempfiles = [f for f in listdir(C1.path_name) if isfile(join(C1.path_name,f))]
        Niter = int(len(C1.list_files))
        #On génère des images en superposant les pellets pris aléatoirement parmis l'ensemble des images
        # Paramètres pour générer les images sont:
        #   - distance min entre deux pellets
        #   - nombre de pellets par image
        #   - ratios bons et mauvais
        labels = []
        count = 0;
        while (Nimg):
            # On préalloue l'ensemble des matrices pour construire l'img
            # on construit l'img en prenant des fichiers training aléatoirement et en recombinant
            img_final = 255*np.ones(C1.imgSize,dtype=np.uint8)
            list_boxes = []
            list_class = []  
            for i1,c in enumerate(C1.pick_obj):
                Ncount = np.ceil(C1.pellets_density*class_ratios[i1]/sum(class_ratios))
                while (Ncount): 
                    #on va trouver une image aléatoirement
                    # On récupère la taille d'une image pour récupérer W et H
                    
                    if (shuffle): 
                        RdmNum = np.random.randint(0, high=c.Nfiles)
                    else: 
                        RdmNum = count
                    img,w0,h0 = c.get_img(RdmNum)    

                    #On trace la box (on suppose l'img centree)
                    l1 = (img[:,:].mean(0) < 255).nonzero() 
                    l2 = (img[:,:].mean(1) < 255).nonzero()
                    recul = 4;
                    box_x0 = l1[0][0] -recul
                    box_w0 = (l1[0][-1]-l1[0][0])+recul*2
                    box_y0 = l2[0][0] -recul
                    box_h0 = (l2[0][-1]-l2[0][0])+recul*2

                    #On positionne le pellet aléatoirement sur la map

                    #on ajoute la box à la liste
                    list_boxes.append([box_x0,box_y0,box_w0,box_h0])
                    x1,y1 = gen_boxCoord(box_w0,box_h0,C1.imgSize)

                    #trouve des coord de box tq il n'y a pas superposition des pellets
                    while(img_final[y1:y1+box_h0,x1:x1+box_w0].mean()<255): 
                        x1,y1 = gen_boxCoord(box_w0,box_h0,C1.imgSize)
                    #on stock les nouvelles coord de la box suite au déplacement du pellet dans l'image
                    boxTemp = [x1,y1,x1+box_w0,y1+box_h0]
                    #On crop l'image d'origine pour
                    img_final[y1:y1+box_h0,x1:x1+box_w0] = img[box_y0:box_y0+box_h0,box_x0:box_x0+box_w0]

                    #On stocke la nouvelle box dans la liste des box
                    list_boxes[-1] = boxTemp
                    #On stocke la classe de l'objet dans la liste des classes
                    list_class.append(c.name)
                    Ncount = Ncount-1

            if (showBckg):
                #on génère des blancs
                hvar = [i3 for i3 in range(40,100)]
                wvar = [i3 for i3 in range(40,100)]

                h = np.random.choice(hvar)
                w = np.random.choice(wvar)
                x1,y1 = gen_boxCoord(w,h,C1.imgSize)

                while(img_final[y1:y1+h,x1:x1+w].mean()<255): 
                    h = np.random.choice(hvar)
                    w = np.random.choice(wvar)
                    x1,y1 = gen_boxCoord(w,h,C1.imgSize)

                boxTemp = [x1,y1,x1+w,y1+h]
                #On stocke la nouvelle box dans la liste des box
                list_boxes.append(boxTemp)
                #On stocke la classe de l'objet dans la liste des classes
                list_class.append('Background')                
            
            #On enregistre l'image
            
            cv2.imwrite(join(C1.path_name,'img','Data%d.png'%(Niter+count)),img_final)
            VOCwrite = PascalVocWriter(join(C1.path_name,'labels',''),filename='Data%d.png'%(Niter+count),imgSize=C1.imgSize,localImgPath=join(C1.path_name,'labels'))
            
            for i1,listbox in enumerate(list_boxes):
                temp = ['Data%d.png'%(Niter+count),listbox[0],listbox[1],listbox[2],listbox[3],list_class[i1]]
                #C1.labels.append(temp)
                VOCwrite.addBndBox(listbox[0],listbox[1],listbox[2],listbox[3],list_class[i1], difficult=0)
                
            VOCwrite.save()
            Nimg = Nimg -1
            count = count +1
        C1.read_path(C1.path_name)

#Class des Data à envoyer au RN     
class DataFlow:
    def __init__(C3,name,SelectDatabase,scale=1):
        C3.name = name
        C3.Database = SelectDatabase
        C3.inputImg = np.array([])
        C3.inputBox = []
        C3.inputClass = []
        C3.scale = scale
        C3.anchorBox = [] #obtained from SS
        
        C3.predClass = []
        C3.predBox = []
        C3.predProba = []
        
    def setFlowFastRCNN(C3,outType,Nbatch=0,num_rois=30,boxfeed='labels',splitData=0,background=0):
        #On recupère le nb de classe
        nb_classes = len(C3.Database.pick_obj)+1
        # On convertit les classes de chaque box, de str vers integer
        inputClass = C3.strClass_2_NumClass(C3.inputClass)
        
        #On met dans une liste les boxs (True box si 'labels' ou via Selective Search autrement)
        if (boxfeed == 'labels'):
            boxAnchors = np.copy(C3.inputBox)
        else:
            boxAnchors = np.copy(C3.anchorBox)
        
        if (outType == 'fit'):
            #Construction des INPUTs
            globalBoxInput = []
            globalLambdaROI = []
            globalLambdaClass = []
            globalBoxAnchor2ROI = []
            globalClassInput = []

            #On parcourt chaque image pour identifier les box et formatté les tableaux pour forward dans le RNN
            
            img = C3.inputImg[Nbatch]
            c = Nbatch
            subInputClass = []
            subBoxAnchors = []
            subInputBox = []
            subLambdaROI = []     
            _boxAnchors = boxAnchors[c] 
        
            #On parcourt les TrueBox
            for c1,boxlabel in enumerate(C3.inputBox[c]): 
                #on calcule l'intersection of Union des Box Proposals avec les True Box
                IoU = bb_intersection_over_union(_boxAnchors, boxlabel)    
                # On trie les IoU et on stack les box dans une liste
                temp = []
                temp1 = []
                for c2,score in enumerate(IoU):
                    if score > 0.7:
                        subBoxAnchors.append(_boxAnchors[c2])
                        subInputClass.append(inputClass[c][c1])
                        subLambdaROI.append(1) #liste avec des 1 pour signifier que cette box devra être evaluée avec la fonction loss
                        subInputBox.append(boxlabel)
            
                    if score < 0.35: #0.1 <
                        temp.append(_boxAnchors[c2]) 
                        temp1.append(boxlabel)
                    if (background &  (score < 0.35)):
                        subBoxAnchors.append(_boxAnchors[c2])
                        subInputClass.append(0)
                        subLambdaROI.append(0) #liste avec des 0 pour ne pas prendre en compte cette box dans l'évaluation dans le loss 
                        subInputBox.append(boxlabel)
    
                if (len(subInputBox) == 0):
                    Nrdm = np.random.randint(len(temp))
                    #print(temp1[Nrdm])
                    #Nrdm2 = np.random.randint(temp[0].shape)
                    subBoxAnchors.append(temp[Nrdm])
                    subInputClass.append(0)
                    subLambdaROI.append(0) #liste avec des 0 pour ne pas prendre en compte cette box dans l'évaluation dans le loss 
                    subInputBox.append(temp1[Nrdm])
            
            # Il faut un nombre fixe de ROI à envoyer, donc on complète si il n'y en a pas assez avec la matrice dupliquée aléatoirement      
            if (num_rois>len(subInputBox)):
                Nrestant = num_rois - len(subInputBox)
                randNlist = np.random.randint(len(subInputBox),size=Nrestant)
                for r1 in randNlist:
                    subBoxAnchors.append(subBoxAnchors[r1])
                    subInputClass.append(subInputClass[r1])
                    subLambdaROI.append(subLambdaROI[r1])
                    subInputBox.append(subInputBox[r1])
            else:     
                subBoxAnchors = subBoxAnchors[0:num_rois]
                subInputClass = subInputClass[0:num_rois]
                subLambdaROI = subLambdaROI[0:num_rois]
                subInputBox = subInputBox[0:num_rois]
    
            # On converti les coordonnées des box proposals en coordonnées normalisées pour l'envoie dans le RNN et on transforme les coor des true box pour la fonction loss
            _boxAnchor2ROI = boxNormalisation2ROIPool(subBoxAnchors,C3.unpackSize) #génère les coord nécessaire pour le ROI pooling (sera envoyé au réseau)
            _boxAnchor2RegNorm = boxNormalisation2Reg(subBoxAnchors,C3.unpackSize) #génère les coords pour calculer les coord transformées pour le Reg du modèle Fast CRNN
            _inputBox2RegNorm = boxNormalisation2Reg(subInputBox,C3.unpackSize) #idem ci-dessus
            _coordBoxParam = coordParametrization(_inputBox2RegNorm,_boxAnchor2RegNorm) #Coord paramétrisées qui sont envoyer au NN
        
            # On convertit les tableau des lambda en tableau numpy et reshape pour RN
            subLambdaROI = np.array(subLambdaROI)
            subLambdaROI = subLambdaROI.reshape(subLambdaROI.shape[0],1) 
        
            # On convertit la liste des inputClass en tableau numpy
            subInputClass = np.array(subInputClass,dtype=np.int32)
        
            #Encoding SubInputClass One Hot if categorical
            temp = np.zeros((subInputClass.shape[0],nb_classes))
            temp[np.arange(subInputClass.shape[0]),subInputClass] = 1
            subInputClass = np.array(temp,dtype=np.int32)
        
        
            # On cree un tableau numpy avec l'ID des images pour chaque box
            _boxAnchor2ROI_Id = np.zeros((_boxAnchor2ROI.shape[0],1))
        
            # On stack horizontalement le tableau des box à envoyer et le tableau de leur ID associée
            temp = np.concatenate((_boxAnchor2ROI,_boxAnchor2ROI_Id),axis=1)
        
            # On ajoute ce tableau des box proposals à une liste (toutes les imgs)
            globalBoxAnchor2ROI.append(temp) #on sort une matrice nTOTROI*5
        
            # On stack horizontalement les coordonnes transformées des true box avec le lambda associé 
            temp = np.concatenate((_coordBoxParam,subLambdaROI),axis=1)
            # On ajoute le tableau des true box dans une liste
            globalBoxInput.append(temp.reshape(1,temp.shape[0],temp.shape[1]))
            # On ajoute le tableau des classes dans une liste
            globalClassInput.append(subInputClass.reshape(1,subInputClass.shape[0],nb_classes)) #nb_classes
        
        
            #On construit le tenseur global des images en normalisant les images de 0 à 1
            imgInput = img.astype('float32')/255 
            imgInput = np.reshape(imgInput,(1,C3.unpackSize[0],C3.unpackSize[1],1))
        
            #On construit le tenseur global de box proposal à mapper sur le ROIPooling
            boxAnchor2ROI = np.concatenate(globalBoxAnchor2ROI).reshape(1,num_rois,5)
            
            #On construit le tenseur global des true box pour l'évluation de la fonction loss Reg
            boxInput = np.concatenate(globalBoxInput)
            #On construit le tenseur global des class pour l'évluation de la fonciton loss class
            classInput = np.concatenate(globalClassInput)
        
            #On retourne les tableaux pour la fonction fit
            inputs = [imgInput[:,:,:,:],boxAnchor2ROI[:,:,:]]
            outputs = [classInput[:,:,:],boxInput[:,:,:]]
        
            return inputs,outputs
    
        else:
            #Construction des INPUTs
            globalImgInput = C3.inputImg
        
            globalBoxAnchor2ROI = []
            globalBoxAnchor2RegNorm = []
        
        
            img = C3.inputImg[Nbatch]
            c = Nbatch
            #subLambdaClass = []     
            subBoxAnchors = [i for i in boxAnchors[c]] 
        
            # Il faut un nombre fixe de ROI à envoyer, donc on complète si il n'y en a pas assez avec la matrice dupliquée aléatoirement      
            if (num_rois>len(subBoxAnchors)):
                Nrestant = num_rois - len(subBoxAnchors)
                randNlist = np.random.randint(len(subBoxAnchors),size=Nrestant)
                for r1 in randNlist:
                    subBoxAnchors.append(subBoxAnchors[r1])
        
            else:     
                subBoxAnchors = subBoxAnchors[0:num_rois]
        
            # On converti les coord des box 
            _boxAnchor2ROI = boxNormalisation2ROIPool(subBoxAnchors,C3.unpackSize) #génère les coord nécessaire pour le ROI pooling (sera envoyé au réseau)
            _boxAnchor2RegNorm = boxNormalisation2Reg(subBoxAnchors,C3.unpackSize)
        
            #globalBoxAnchor2ROI.append(_boxAnchor2ROI.reshape(1,_boxAnchor2ROI.shape[0],_boxAnchor2ROI.shape[1]))  
            _boxAnchor2ROI_Id = np.zeros((_boxAnchor2ROI.shape[0],1))
        
            temp = np.concatenate((_boxAnchor2ROI,_boxAnchor2ROI_Id),axis=1)
            globalBoxAnchor2ROI.append(temp) #on sort une matrice nTOTROI*5
            globalBoxAnchor2RegNorm.append(_boxAnchor2RegNorm.reshape(1,_boxAnchor2RegNorm.shape[0],_boxAnchor2RegNorm.shape[1]))  
        
            #On construit le tenseur global des images en normalisant les images de 0 à 1
            imgInput = img.astype('float32')/255 
            imgInput = np.reshape(imgInput,(1,C3.unpackSize[0],C3.unpackSize[1],1))
            
            #On construit le tenseur global de box à mapper sur le ROIPooling + regNorm pour l'inversion des coord du regresseur predites
            boxAnchor2ROI = np.concatenate(globalBoxAnchor2ROI).reshape(1,num_rois,5)
            boxAnchor2RegNorm= np.concatenate(globalBoxAnchor2RegNorm)
            
            inputs = [imgInput,boxAnchor2ROI]
            boxAnchor2RegNorm
            return inputs, boxAnchor2RegNorm
    
    def strClass_2_NumClass(C3,listClass): #Conversion 
        #numC,_ = self.dict_ClassObj[list_class[count]]
        inputClassNum = []
        for c,row_listClass in enumerate(listClass):
            temp = [C3.Database.classDef[className][0] for className in row_listClass]
            inputClassNum.append(temp)
        return inputClassNum

    def genBatch(C3,Nbatch,shuffle=True):
        C3.Nbatch = Nbatch
        list_N = [i for i in range(1,C3.Database.Nfiles)]
        if (shuffle):
          np.random.shuffle(list_N)
        C3.unpackSize = (int(C3.Database.imgSize[0]/C3.scale),int(C3.Database.imgSize[1]/C3.scale))
        C3.inputImg = C3.Database.get_img(num=list_N[0:Nbatch],resize=C3.unpackSize)
        C3.inputBox,C3.inputClass = C3.Database.get_labels(num=list_N[0:Nbatch],scale=C3.scale)

    def genAnchors(C3):    
        ROIProposal = []
        for count in range(0,C3.inputImg.shape[0]):
            temp = C3.inputImg[count,:].reshape(C3.unpackSize)
            temp = cv2.cvtColor(temp, cv2.COLOR_GRAY2BGR)
            ROIProposal.append(ROI_proposal(temp))
        C3.anchorBox = ROIProposal

    def setPred(C3,output,boxAnchor2RegNorm,th=0.5): #,list_output,list_candidates
        
        #On decode les class et les coordonnées et 
        tempProb = output[0]
        tempMax = np.argmax(tempProb,axis=2)
        tempBox = output[1]
        
        # On met dans un dictionnaire pour rester compatible avec code tensorflow low api
        
        classProbOut = tempProb.tolist()
        classOut = tempMax.tolist()
        t = {}
        output = []
        candidates = []
        for i in range(len(classOut)):
            #inversion des coords
            boxOutput = invCoordParametrization(tempBox[i],boxAnchor2RegNorm[i])
            boxOutput = invBoxNormalisation2Reg(boxOutput,C3.unpackSize)   
            
            #construction du dict
            t = {'classes': classOut[i], 'boxCoord': boxOutput, 'probabilities': classProbOut[i]}
            refineBox = np.hstack((boxOutput,classProbOut[i]))
            candidates.append(non_maximum_supression(refineBox, th))
            #stockage du dict
            output.append(t)
        
        list_output = output
        list_candidates = candidates
        
        C3.predClass = [i['classes'] for i in list_output]
        C3.predBox = [i['boxCoord'] for i in list_output]

        C3.predProba = [i['probabilities'] for i in list_output]
        C3.outputCandidates = [i for i in list_candidates]
    
    def getPred(C3,mat2select,listCandidates):
        temp = []
        for c,mat in enumerate(mat2select):
            if (type(mat) is list):
                temp.append([mat[i] for i in listCandidates[c]])
            else:
                temp.append([mat[i] for i in listCandidates[c]])
        return temp
