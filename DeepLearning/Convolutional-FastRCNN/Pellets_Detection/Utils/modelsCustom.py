import tensorflow as tf

class customModel():
    def __init__(self,path='',inputSize=(200,200,1),Nclass=3):
        self.inputSize = inputSize
        self.Nclass = Nclass;
        self.graph = tf.Graph()
        self.create_graph(path)
        
        
    def create_graph(self,path):
        with self.graph.as_default():
            #Definition des tenseurs I/O
            
            
            self.inputImg = tf.placeholder(dtype=tf.float32, shape=(None,self.inputSize[1]*self.inputSize[0]),name='tf_inputImg')
            
            
            self.x = tf.reshape(self.inputImg,shape=tf.constant([-1,self.inputSize[1],self.inputSize[0],self.inputSize[2]],dtype=tf.int32),name='tf_x')

            self.inputBox = tf.placeholder(dtype=tf.float32, shape=(None,4),name='tf_inputBox')

            self.inputBoxId = tf.placeholder(dtype=tf.int32, shape=None,name='tf_inputBoxId')
            
            self.yBox = tf.placeholder(dtype=tf.float32, shape=(None, 4),name='tf_yBox')
            
            self.yClass = tf.placeholder(dtype=tf.int32, shape=None,name='tf_yClass')
            self.beta = tf.placeholder(dtype=tf.float32, shape=(None,1),name='beta')
            
            self.learn_rate = tf.placeholder(dtype=tf.float32)
            
            #---------- Construction du DNN --------
            #layer1
            layer1_conv = self.create_conv_layer(name='conv', layer_scope='layer1',input=self.x,kernel_size=(7,7),stride=(2,2),Nfilters=32)
            layer1_maxpool = self.create_maxpool_layer(name='maxpool',layer_scope='layer1',input=layer1_conv,kernel_size=(3,3),stride=(2,2))
        
            #layer2
            layer2_conv = self.create_conv_layer(name='conv', layer_scope='layer2',input=layer1_maxpool,kernel_size=(5,5),stride=(2,2),Nfilters=80)
            layer2_maxpool = self.create_maxpool_layer(name='maxpool',layer_scope='layer2',input=layer2_conv,kernel_size=(3,3),stride=(2,2))
    
            #layer3
            layer3_conv = self.create_conv_layer(name='conv', layer_scope='layer3',input=layer2_maxpool,kernel_size=(3,3),stride=(1,1),Nfilters=100)
            
            #Decoupage du layer3 conformément à la box fournie en input => output a une taille fixe ([NBox,7,7,Nfilters]
            layer4_ROIPooling = tf.image.crop_and_resize(layer3_conv,self.inputBox,self.inputBoxId,crop_size=(7,7),name='layer4')
            #Reshape
            layer4_reshape = tf.reshape(layer4_ROIPooling, [-1, 100*7*7])
            
            #Fully connected Network (dense Layers)
            layer5_dense = self.create_dense_layer(input=layer4_reshape,Noutput=1000,name='layer5',layer_scope='layer5')
			layer5b_dense = self.create_dense_layer(input=layer5_dense,Noutput=1000,name='layer5b',layer_scope='layer5')
            
            #Connexion des détecteurs 
            #Classification 
            layer6_classification = self.create_dense_layer(input=layer5b_dense,Noutput=self.Nclass,name='layer6_classification',layer_scope='layer6')
            #Box
            layer6_box = self.create_dense_layer(input=layer5_dense,Noutput=4,name='layer6_box',layer_scope='layer6')
            
            #----------- On garde les derniers Tenseurs
            self.outputClassification = layer6_classification
            classTensor =tf.argmax(self.outputClassification,axis=1,output_type=tf.int32)
                        
            #----------- Optimisation
            #zerosTensor = tf.zeros(shape=tf.shape(classTensor),dtype=tf.int32)
            #condZeros = tf.cast(tf.not_equal(classTensor,zerosTensor),dtype=tf.float32)
            #condZeros = tf.reshape(condZeros,[tf.shape(classTensor),1])
            self.outputBox = layer6_box
            
            self.boxLoss = 30*self.box_loss(tf.multiply(self.beta,self.yBox), tf.multiply(self.beta,layer6_box)) #self.yBox
            self.class_loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=layer6_classification, labels=self.yClass))
            #self.loss_ = tf.cond(tf.add(self.box_loss, self.class_loss)

            probabilitiesTensor = tf.nn.softmax(self.outputClassification,name='softmax_tensor')
            #self.loss_ = tf.add(self.box_loss, tf.multiply(classTensor,self.class_loss))
#            self.loss_ = tf.add(self.box_loss,tf.multiply(condZeros,self.class_loss))
            self.loss_ = tf.add(self.boxLoss,self.class_loss)
            #self.loss_ = tf.add(tf.cond(self.beta,self.boxLoss),self.class_loss)
            #self.loss_ = tf.add(self.boxLoss,self.class_loss)
            self.opt = tf.train.MomentumOptimizer(self.learn_rate, 0.9).minimize(self.loss_)

            #----------- On construit un dico utile
            self.Func_output = {'classes' : classTensor, 'probabilities' : probabilitiesTensor, 'boxCoord': self.outputBox}
            self.Func_prediction = {'classes' : classTensor,'probabilities' : probabilitiesTensor, 'boxCoord': self.outputBox, 'accuracy_class':self.loss_,'accuracy_class':self.class_loss,'accuracy_box':self.boxLoss}
                        
            self.saveModel = tf.train.Saver()            
            
    def smooth_l1(self, x, y):
        def smooth_abs(u):
            return tf.cond(tf.less(tf.abs(u), 1.), lambda: 0.5*u**2, lambda: tf.abs(u) - 0.5)
        z = (x - y)
        smooth_vals = tf.map_fn(smooth_abs, z)
        return tf.reduce_mean(smooth_vals)

    def box_loss(self, x, y):
        loss = (self.smooth_l1(x[:, 0], y[:, 0]) + self.smooth_l1(x[:, 1], y[:, 1])
                + self.smooth_l1(x[:, 2], y[:, 2]) + self.smooth_l1(x[:, 3], y[:, 3]))
        return loss

    def create_dense_layer(self,input,Noutput,name='default',layer_scope='default_scope_name'):
        with tf.variable_scope(layer_scope):
            input1 = tf.layers.dropout(input,rate=0.2)
            dense = tf.layers.dense(inputs=input1, units=Noutput,name=name)
            f = tf.nn.tanh(dense)
            return f
        
    def create_conv_layer(self,input,name='default',layer_scope='default_scope_name',Nfilters=1,stride=(1,1),kernel_size=(5,5)):
        with tf.variable_scope(layer_scope):
            #conv = tf.nn.conv2d(prev_layer, filter, strides=[1, stride, stride, 1], padding='SAME',name=name)
            conv = tf.layers.conv2d(inputs=input, filters=Nfilters, kernel_size=kernel_size,strides=stride, padding='SAME',name=name)
            f = tf.nn.relu(conv)
            return f
        
    def create_maxpool_layer(self,input,name='default',layer_scope='default_scope_name',kernel_size=(2,2),stride=2):
        with tf.variable_scope(layer_scope):
            pool = tf.layers.max_pooling2d(inputs=input, pool_size=kernel_size, strides=stride,name=name)
            f = tf.nn.relu(pool)
            return f