#%%  all imports and setup configuration
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Layer, Activation,Dense, Dropout
from tensorflow.keras.optimizers import Adam, SGD, RMSprop

import pandas as pd
import numpy as np
import os

from utils.genGraphMol import *
from utils.functions import *

import csv

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import random

from matplotlib import pyplot as plt

from hyperopt import fmin, hp, tpe #parametre opt

# dataRead
PATH_DATASET_TEST = 'data/QM9_test2.csv'
PATH_DATASET_TRAINING = 'data/QM9_train2.csv'

SEP = ','
DATASET_SPLIT_TRAINING = 0.8

# Learning Parameters
N_EPOCH = 40
TRAINING = True
TEST = False
BATCH_SIZE = 20

# Model Parameters
MODEL_CKPT_PATH = './tf_ckpts_hidden_600_qm9'
MODEL_DROPOUT = 0.3
MODEL_HIDDEN_SIZE = 600
MODEL_ACTIVATION = 'Relu'
MODEL_DEPTH = 5
MODEL_DIRECTED = False
MODEL_FFN_HIDDEN_SIZE = 500,550
MODEL_LEARNING_RATE = 0.0001


#%% Dataset Loading and scaling
# Load dataset

csv_path = os.path.join(PATH_DATASET_TRAINING)
d_training = pd.read_csv(csv_path,sep=SEP)

csv_path = os.path.join(PATH_DATASET_TEST)
d_test = pd.read_csv(csv_path,sep=SEP)


#d_training.fillna('-')
#d_training = d_training[d_training['smiles'] != '-']

xtrain = d_training['smiles']
xtest= d_test['smiles']


ytrain = np.asarray(d_training.iloc[:,1:])
ytest= np.asarray(d_test.iloc[:,1:])

#We normalized training/test dataset
scaler = StandardScaler()
scaler.fit(ytrain)
ytrain = scaler.transform(ytrain)
ytest = scaler.transform(ytest)

# Split Training dataset for validation with a random
idxTrain = np.arange(ytrain.shape[0])
np.random.shuffle(idxTrain)
xtrain = xtrain.iloc[idxTrain]
ytrain = ytrain[idxTrain]

xtrain, xval, ytrain, yval = train_test_split(xtrain, ytrain, test_size=1-DATASET_SPLIT_TRAINING)

#%% Model construction
#----------------------------------------

# We build the model of MPNN
m_fingerprint = [D_MPNN(ATOM_FDIM,BOND_FDIM,MODEL_HIDDEN_SIZE,MODEL_DIRECTED,MODEL_DEPTH,MODEL_DROPOUT)] #MPNN layer
m_nn = []
for ffn_hidden in MODEL_FFN_HIDDEN_SIZE: # layers of NN
    m_nn.append(Dense(ffn_hidden,activation='relu'))
    m_nn.append(Dropout(MODEL_DROPOUT))

m_nn.append(Dense(ytrain.shape[1]))
m_fingerprint.extend(m_nn)
m_global = keras.Sequential(m_fingerprint)

# Model training parameters
optimizer = tf.keras.optimizers.Adam(learning_rate=MODEL_LEARNING_RATE)

#%% Training
# Training  step
if (TRAINING):
    # Loading previous model
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), optimizer=optimizer, m_global=m_global)
    manager = tf.train.CheckpointManager(ckpt,MODEL_CKPT_PATH, max_to_keep=3)
    ckpt.restore(manager.latest_checkpoint)
    if manager.latest_checkpoint:
        print("Restored from {}".format(manager.latest_checkpoint))
    else:
        print("Initializing from scratch.")
    
    # Training loop
    err_prev = 100
    for epoch in range(N_EPOCH):
        print('Iteration #: '+str(epoch))
        # We random the training set
        idxTrain = np.arange(ytrain.shape[0])
        np.random.shuffle(idxTrain)
        xtrain = xtrain.iloc[idxTrain]
        ytrain = ytrain[idxTrain]

        nbatch = int(np.ceil(ytrain.shape[0]/BATCH_SIZE))
        err_training = [] 
            #use to save model if better than previous epoch
        for n in range(nbatch): 
            idx_lower = n*BATCH_SIZE #lower Idx to map in the trainDataset
            idx_upper = (n+1)*BATCH_SIZE #upper index to map in the trainDataset
            if (idx_upper > xtrain.shape[0]):
                idx_upper = xtrain.shape[0]-1 #if upper index is outside the size of the trainDataset, we take the last element

            listSmiles = list(xtrain.iloc[idx_lower:idx_upper].values) # We generate a list of string
            batchGraph = mol2graph(listSmiles) # we generate an object

            # We create the input tensor
            f_atoms, f_bonds, a2b, b2a, b2revb, a_scope, b_scope = batchGraph.get_components()
            
            #print(f_bonds.shape,a2b.shape)
            x = f_atoms,f_bonds,a2b,b2a,b2revb,a_scope,b_scope
            
            # we get the targets for this batch
            y = ytrain[idx_lower:idx_upper] 
        
            grads,loss_value_training = gradModel(m_global,x,y) # We calculate the gradients
            optimizer.apply_gradients(zip(grads, m_global.trainable_variables)) # We apply the correction to weight according to gradients
            err_training.append(tf.reduce_mean(loss_value_training,axis=0).numpy())
            printProgressBar(n + 1, nbatch, prefix = 'Progression of the Epoch:', suffix = 'Complete | error (MSE): '+str(np.mean(err_training)), length = 50)
        # comput metric for test dataset
        listSmiles = list(xtest.values) # We generate a list of string
        batchGraph = mol2graph(listSmiles) # we generate an object
        f_atoms, f_bonds, a2b, b2a, b2revb, a_scope, b_scope = batchGraph.get_components()
        x = f_atoms,f_bonds,a2b,b2a,b2revb,a_scope,b_scope
        y = ytest 
        grads,loss_value_training = gradModel(m_global,x,y)
        err_test = tf.reduce_mean(loss_value_training,axis=0).numpy()
        print('Avg error on training: ',np.mean(err_training),' Avg error on test: ',err_test)
        #te model is saved if better than previous
        #we only save the best model
        if (err_prev >= err_test):
            ckpt.step.assign_add(1)
            save_path = manager.save()
            err_prev = err_test 
            print("Saved checkpoint for step {}: {}".format(int(ckpt.step), save_path))
#%% Test
if (TEST):
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), optimizer=optimizer, m_global=m_global)
    manager = tf.train.CheckpointManager(ckpt, MODEL_CKPT_PATH, max_to_keep=3)
    ckpt.restore(manager.latest_checkpoint)
    listSmiles = list(xtest.values) # We generate a list of string
    batchGraph = mol2graph(listSmiles) # we generate an object
    f_atoms, f_bonds, a2b, b2a, b2revb, a_scope, b_scope = batchGraph.get_components()

    x = f_atoms,f_bonds,a2b,b2a,b2revb,a_scope,b_scope
    y = ytest 
    prediction = m_global(x)
    
    y = scaler.inverse_transform(y)
    prediction = scaler.inverse_transform(prediction)    

    plt.plot(y[:,0], prediction[:,0],'b*')
    plt.plot(y[:,1], prediction[:,1],'r*')
    plt.plot(y[:,2], prediction[:,2],'g*')

    plt.show()
    np.savetxt("data/output.csv", np.concatenate((y,prediction),axis=1), delimiter=",")