
import tensorflow as tf
from tensorflow import keras

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '')
    # Print New Line on Complete
    if iteration == total: 
        print()

class D_MPNN(tf.keras.Model):
    def __init__(self,atom_fdim, bond_fdim,MODEL_HIDDEN_SIZE,MODEL_DIRECTED,MODEL_DEPTH,MODEL_DROPOUT):
        super(D_MPNN, self).__init__()
        
        #loading pars
        self.atom_fdim = atom_fdim
        self.bond_fdim = bond_fdim
        self.hidden_size = MODEL_HIDDEN_SIZE
        self.undirected = MODEL_DIRECTED
        # Cached zeros
        self.cached_zero_vector = tf.zeros(self.hidden_size)
        self.depth = MODEL_DEPTH
        # Input
        input_dim = self.bond_fdim

        # init layer
        #input_shape=(input_dim,)
        self.nn_i = tf.keras.layers.Dense(self.hidden_size,use_bias=True)
        
        # Shared weight matrix across depths (default)
        w_h_input_size = self.hidden_size

        self.nn_h = tf.keras.layers.Dense(self.hidden_size,use_bias=True)
        self.nn_o = tf.keras.layers.Dense(self.hidden_size, input_shape=(self.atom_fdim + self.hidden_size,),use_bias=True)
        self.nn_dropout = tf.keras.layers.Dropout(rate=MODEL_DROPOUT)
    def index_select_ND(self,source, index):
        """
        Selects the message features from source corresponding to the atom or bond indices in index.
        :param source: A tensor of shape (num_bonds, hidden_size) containing message features.
        :param index: A tensor of shape (num_atoms/num_bonds, max_num_bonds) containing the atom or bond
        indices to select from source.
        :return: A tensor of shape (num_atoms/num_bonds, max_num_bonds, hidden_size) containing the message
        features corresponding to the atoms/bonds specified in index.
        """
        index_size = index.shape  # (num_atoms/num_bonds, max_num_bonds)
        suffix_dim = source.shape[1:]  # (hidden_size,)
        final_size = index_size + suffix_dim  # (num_atoms/num_bonds, max_num_bonds, hidden_size)
        
        target = tf.gather(source,tf.reshape(index,[-1])) # (num_atoms/num_bonds * max_num_bonds, hidden_size) 
        #source.index_select(dim=0, index=)
        #print(target.shape,index_size,source.shape)  
        target = tf.reshape(target,[final_size[0],final_size[1],final_size[2]])
        #view(final_size)  # (num_atoms/num_bonds, max_num_bonds, hidden_size)

        return target
        
    def call(self, inputs):
        # loading of the tensors
        f_atoms = inputs[0]
        f_bonds = inputs[1]
        a2b = inputs[2]
        b2a = inputs[3]
        b2revb = inputs[4]
        a_scope = inputs[5]
        b_scope = inputs[6]
        
        inputMess = self.nn_i(f_bonds)
        message = tf.nn.relu(inputMess)
         
        #print(f_bonds.shape,message.shape,self.hidden_size,a2b.shape)
        for depth in range(self.depth - 1):
            if self.undirected:
                message = (message + tf.gather(message,b2revb)) / 2
            nei_a_message = self.index_select_ND(message, a2b) # n_atomsxhidden
            a_message = tf.reduce_mean(nei_a_message,axis=1)
            rev_message = tf.gather(message,b2revb)  # num_bonds x hidden
            message = tf.gather(a_message,b2a) - rev_message  # num_bonds x hidden
            
            message = self.nn_h(message)
            message = tf.nn.relu(inputMess + message)  # num_bonds x hidden_size
            message = self.nn_dropout(message)  # num_bonds x hidden

        a2x = a2b
        nei_a_message = self.index_select_ND(message, a2x) #num_atoms x max_num_bonds x hidden
        a_message = tf.reduce_mean(nei_a_message,axis=1) # num_atoms x hidden
        a_input = tf.keras.layers.concatenate([f_atoms, a_message], axis=1)  #num_atoms x (atom_fdim + hidden)
        atom_hiddens = tf.nn.relu(self.nn_o(a_input))  # num_atoms x hidden
        atom_hiddens = self.nn_dropout(atom_hiddens)  # num_atoms x hidden

                # Readout
        mol_vecs = []
        for idx, (a_start, a_size) in enumerate(a_scope):
            a_start = a_start.numpy()
            a_size = a_size.numpy()
            #print(a_start,a_size)
            if a_size == 0:
                mol_vecs.append(self.cached_zero_vector)
            else:
                cur_hiddens = atom_hiddens[a_start:a_start+a_size]
                
                #tf.slice(atom_hiddens, a_start, a_size)
                mol_vec = cur_hiddens  # (num_atoms, hidden_size)

                mol_vec = tf.reduce_mean(mol_vec,axis=0)/a_size
                
                #mol_vec.sum(dim=0) / a_size
                mol_vecs.append(mol_vec)

        mol_vecs = tf.stack(mol_vecs, axis=0)  # (num_molecules, hidden_size)
            #print(message.shape)
        return mol_vecs

def gradModel(model,inputs,targets):
    with tf.GradientTape() as tape:
        y = model(inputs, training=True)
        loss_value = tf.losses.mean_squared_error(targets,y)
    return tape.gradient(loss_value, model.trainable_variables),loss_value