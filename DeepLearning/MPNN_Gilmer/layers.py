# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 17:13:59 2019

@author: BBOUVIE1
"""


from keras import backend as K
from keras.layers import Layer

class MPNN(Layer):
    
    def __init__(self, dimLayers=[200,75], Tsteps=5, **kwargs):   
        self.Tsteps = Tsteps
        self.dimLayers = dimLayers
        self.nHiddenLayersEdges =len(dimLayers)
        super(MPNN, self).__init__(**kwargs)
             
#        define weights of Layer
    def build(self, input_shape):
        self.n_atom = input_shape[0][1]
        self.nAtomsFeatures = input_shape[0][2]
        self.nBondsFeatures = input_shape[1][3]
      
        nInput = [self.nBondsFeatures]
        nOutput = [self.nAtomsFeatures*self.nAtomsFeatures]
        
        hidden_layer = ["hidden_layer"+str(i+1) for i in range(self.nHiddenLayersEdges)]
        hidden_layer.append("output_layer")
        self.dimLayers_ = np.concatenate((nInput, self.dimLayers, nOutput))
        
        self.mapEdgesWeights = []
        self.mapEdgesBias = []
        
#############################  edges NN params

        for i in range(self.nHiddenLayersEdges + 1):

            self.mapEdgesWeights.append( self.add_weight(shape=(int(self.dimLayers_[i]), int(self.dimLayers_[i+1])),
                                          dtype=np.float32, initializer='glorot_uniform', name=hidden_layer[i]+"_Weights"))
            
            self.mapEdgesBias.append( self.add_weight(shape=([self.dimLayers_[i+1]]), dtype=np.float32,
                                          initializer='zeros', name=hidden_layer[i]+"_Bias"))
        
#############################  GRU params

        GRUGates = ["GRU_rGate","GRU_zGate", "GRU_forgetGate",]
        self.GRU_inputsWeights = []
        self.GRU_statesWeights = []
        self.GRU_Bias = []

        for gate in GRUGates :
            self.GRU_inputsWeights.append( self.add_weight(shape=(self.nAtomsFeatures, self.nAtomsFeatures),
                                              dtype=np.float32, initializer='glorot_uniform', name=gate+"inputsWeights"))
            
            self.GRU_statesWeights.append( self.add_weight(shape=(self.nAtomsFeatures, self.nAtomsFeatures),
                                  dtype=np.float32, initializer='glorot_uniform', name=gate+"statesWeights"))
            
            self.GRU_Bias.append( self.add_weight(shape=[self.nAtomsFeatures],
                                  dtype=np.float32, initializer='zeros', name=gate+"Bias"))   


        super(MPNN, self).build(input_shape)
        
    def call(self, inputs):
      
        state_molecules, edges = inputs
        
#######################  edges NN    

        hidden_state = edges
        for i in range(self.nHiddenLayersEdges+1):
            hidden_state = K.dot(hidden_state, self.mapEdgesWeights[i]) + self.mapEdgesBias[i]
            # no activation on output layer
            if (i!=self.nHiddenLayersEdges):
                hidden_state = K.relu(hidden_state)
        outputNNedges = K.reshape(hidden_state, shape=(-1, self.n_atom, self.n_atom, self.nAtomsFeatures, self.nAtomsFeatures))
        
######################  compute message and update state

        states = state_molecules*0

        for step in range(self.Tsteps):
            repeat_state_molecules = K.reshape(K.repeat_elements(state_molecules, self.n_atom, axis=1), shape=(-1,self.n_atom,self.n_atom,self.nAtomsFeatures))
            messages = K.sum(K.batch_dot(repeat_state_molecules, outputNNedges, axes=[1,2]), axis=1)
            state_molecules = self.GRU(messages, states)  
            states = state_molecules
    
######################  apply linear projection to state molecules after steps on initial state molecules

        state_projection = K.sum(inputs[0] * state_molecules, axis=2)
        
        return state_projection

    
    def GRU(self, x, h):
            
        z = K.sigmoid(K.dot(x, self.GRU_inputsWeights[0]) + K.dot(h, self.GRU_statesWeights[0]) + self.GRU_Bias[0])
        r = K.sigmoid(K.dot(x, self.GRU_inputsWeights[1]) + K.dot(h, self.GRU_statesWeights[1]) + self.GRU_Bias[1])
        g = K.tanh(K.dot(x, self.GRU_inputsWeights[2]) + K.dot(r*h, self.GRU_statesWeights[2]) + self.GRU_Bias[2])
        y = z*h + (1-z)*g

        return y  

    def compute_output_shape(self, input_shape):  
        return ((input_shape[0][0], input_shape[0][1]))
    

    def get_config(self):
        config = {'Tsteps': self.Tsteps,
                  'dimLayers': self.dimLayers}
        base_config = super(MPNN, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
        

class set2set(Layer):
    
    def __init__(self, memorySize=300, Msteps=5, **kwargs):
        self.Msteps = Msteps
        self.memorySize = memorySize
        super(set2set, self).__init__(**kwargs)
             
#        define weights of Layer
    def build(self, input_shape):
        self.n_atom = input_shape[1]
      
#############################  memory NN params
        
        self.mapMemoryWeights = []
        self.mapMemoryBias = []
        
        
        
        self.mapMemoryWeights.append( self.add_weight(shape=(self.memorySize, self.memorySize),
                                          dtype=np.float32, initializer='glorot_uniform', name="hidden_layer1_Weights"))
        
        self.mapMemoryWeights.append( self.add_weight(shape=(self.memorySize, self.memorySize),
                                          dtype=np.float32, initializer='glorot_uniform', name="output_layer_Weights"))
        
        self.mapMemoryBias.append( self.add_weight(shape=([self.memorySize]), dtype=np.float32,
                                          initializer='zeros', name="hidden_layer1_Bias"))
        
        self.mapMemoryBias.append( self.add_weight(shape=([self.memorySize]), dtype=np.float32,
                                          initializer='zeros', name="output_layer_Bias"))
        
        
#############################  Custom LSTM params

        LSTMGates = ["LSTM_forgetGate", "LSTM_inputGate", "LSTM_outputGate", "LSTM_mainGate"]
        self.LSTM_Weights = []
        self.LSTM_Bias = []

        for gate in LSTMGates :
            self.LSTM_Weights.append( self.add_weight(shape=(2*self.memorySize, self.memorySize),
                                              dtype=np.float32, initializer='glorot_uniform', name=gate+"Weights"))
            
            self.LSTM_Bias.append( self.add_weight(shape=[self.memorySize],
                                  dtype=np.float32, initializer='zeros', name=gate+"Bias"))   

        
        super(set2set, self).build(input_shape)
        
    def call(self, inputs):
          
        state_molecules_projection = inputs       
        
#######################  memory NN

        # repeat_state_molecules_projection has shape batch_size * n_atom * memorySize
        repeat_state_molecules_projection = K.dot(K.expand_dims(state_molecules_projection, axis=2), tf.ones(shape=(1, self.memorySize)))
        hidden_state = K.dot(repeat_state_molecules_projection, self.mapMemoryWeights[0]) + self.mapMemoryBias[0]
        hidden_state = K.relu(hidden_state)
        memory = K.dot(hidden_state, self.mapMemoryWeights[1]) + self.mapMemoryBias[1]

#######################  attention mechanism
        
        c = 0*K.sum(memory, axis=1)
        q_star_molecules = 0*K.concatenate((c,c), axis=1)

        for step in range(self.Msteps):
            q, c = self.LSTM(q_star_molecules, c)
            e = K.batch_dot(memory, q, axes=[2,1])
            a = K.softmax(e, axis=1)
            r = K.sum(K.expand_dims(a, axis=2)*memory, axis=1)
            q_star_molecules = K.concatenate([q, r], axis=1)
                          
        return q_star_molecules 
      
    
    def LSTM(self, h, c):
        f = K.sigmoid(K.dot(h, self.LSTM_Weights[0]) + self.LSTM_Bias[0])
        i = K.sigmoid(K.dot(h, self.LSTM_Weights[1]) + self.LSTM_Bias[1])
        o = K.sigmoid(K.dot(h, self.LSTM_Weights[2]) + self.LSTM_Bias[2])
        g = K.tanh(K.dot(h, self.LSTM_Weights[3]) + self.LSTM_Bias[3])        
        next_c = f*c + i*g
        next_q = o*K.tanh(next_c)

        return next_q, next_c
        
    
    def compute_output_shape(self, input_shape):  
        return (input_shape[0], 2*self.memorySize)
        
    
    def get_config(self):
        config = {'Msteps': self.Msteps,
                  'memorySize': self.memorySize}
        base_config = super(set2set, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
        

